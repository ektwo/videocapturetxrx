#pragma once
#include <windows.h>
#include "./common/abstractcamera.h"

class CaptureWindow
{

public:
	CaptureWindow(ECaptureDeviceType deviceType);
	~CaptureWindow() {};

	void Show();
	void DrawCapture(int x, int y, int width, int height, int bpp, unsigned char* data);

protected:
	static CaptureWindow* instance;
	void MessageLoop();

private:
	ECaptureDeviceType m_deviceType;
	HWND	window;
	HDC		backbuffer;
	HBITMAP	backbitmap;

	static LRESULT CALLBACK MessageHandler(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
};