#include "stdafx.h"
#include "CaptureWindow.h"

#include <fcntl.h>
#include <io.h>
#include <thread>
#include <mutex>
#include <condition_variable>

#include <stdio.h>
#include <conio.h>
#include "./common/common.h"
#include "./hmx/hmx_lib.h"
#include "camera_api_etron.h"

/* #define DEBUG */
//#define SHOW_IMG_WIN 
#define USE_IPC_CMD_CTRL
//#define USE_IPC_ERROR

#define NIR_IMG_WIDTH		1280
#define NIR_IMG_HEIGHT		720
#define NIR_IMG_CHANNEL		3
#define NIR_IMG_DEPTH		1
#define NIR_IMG_SIZE		(NIR_IMG_WIDTH*NIR_IMG_HEIGHT*NIR_IMG_CHANNEL*NIR_IMG_DEPTH)
#define DEP_IMG_WIDTH		1280//216
#define DEP_IMG_HEIGHT		720//124
#define DEP_IMG_CHANNEL		1
#define DEP_IMG_DEPTH		2
#define DEP_IMG_SIZE		(DEP_IMG_WIDTH*DEP_IMG_HEIGHT*DEP_IMG_CHANNEL*DEP_IMG_DEPTH)
#define DEP_IMG_ORIG_WIDTH	(DEP_IMG_WIDTH - 1)
#define DEP_IMG_ORIG_HEIGHT	(DEP_IMG_HEIGHT - 1)

#define CMD_LENGTH	7

using namespace std;

namespace {
	enum CMD {
		CMD_NONE = 0,
		CMD_REQ_NIR = 0x01 << 0,
		CMD_REQ_DEP = 0x01 << 1,
		CMD_REQ_IMG = CMD_REQ_NIR | CMD_REQ_DEP,
		CMD_QUIT = 0x01 << 2
	};
};

/* #######################
 * ##    Global Vars    ##
 * ####################### */

volatile bool _gblRunning = true;
int _gCmd = 0;
mutex _gMtxCmd;
condition_variable _gCondCmd;
CaptureWindow *_gpCw = NULL;
FILE *_gfpLog = NULL;
unsigned char DepImg[DEP_IMG_SIZE] = { 0 };

ECaptureDeviceType g_deviceType = e_devicetype_etron_3d_camera;

#define Log(format, ...) fprintf (_gfpLog, "[VideoCaptureTransmitter] "format, ##__VA_ARGS__)

#ifdef DEBUG
#define LogDebug(...)	Log(##__VA_ARGS__)
#else
#define LogDebug(...)
#endif

/* ############################
 * ##    Static Functions    ##
 * ############################ */
int OpenCamera()
{
	int ret = -1;

	if (e_devicetype_etron_3d_camera == g_deviceType)
	{
		ret = OpenCamera_ETron(0);
	}
	else if (e_devicetype_hmx_3d_camera == g_deviceType)
	{
		//if (ret_PASS != init_lib()) {
		//	Log("Failed to init lib.\n");
		//}
		//else
		//{
		//	if ((ret_PASS != camera_open(RGB_CAM, NIR_IMG_WIDTH, NIR_IMG_HEIGHT)) ||
		//		(ret_PASS != camera_open(DEPTH_CAM, DEP_IMG_ORIG_WIDTH, DEP_IMG_ORIG_HEIGHT))) {
		//		Log("Failed to open camera.\n");
		//	}
		//	else
		//		ret = 0;
		//}
	}

	DSG(1, "OpenCamera ret=%d", ret);

	return ret;
}

void CloseCamera()
{
	if (e_devicetype_etron_3d_camera == g_deviceType)
	{
		CloseCamera_ETron();
	}
	else if (e_devicetype_hmx_3d_camera == g_deviceType)
	{
		//deinit_lib();
	}
}

void StartCamera()
{
	if (e_devicetype_etron_3d_camera == g_deviceType)
	{
		StartCamera_ETron();
	}
	else if (e_devicetype_hmx_3d_camera == g_deviceType)
	{
	}
}

void StopCamera()
{
	if (e_devicetype_etron_3d_camera == g_deviceType)
	{
		StopCamera_ETron();
	}
	else if (e_devicetype_hmx_3d_camera == g_deviceType)
	{
	}
}


/* @brief Open log file */
static void OpenLog()
{
	/* fopen_s(&_gfpLog, "log.txt", "w"); */
	_gfpLog = stderr;
	setvbuf(_gfpLog, NULL, _IONBF, 0);
	Log("Start main\n");
}

/* @brief Blocking wait until command received or stop running */
static bool _WaitCmds(int Mask, int Val)
{
	unique_lock<mutex> lock(_gMtxCmd);

	//_gCondCmd.wait(lock, [=] { return ((false == _gblRunning) || ((true == _gblRunning) && (_gCmd & Mask) == Val)); });
	//_gCondCmd.wait(lock, [=] { return ((false == _gblRunning) || ((_gCmd & Mask) == Val)); });
	_gCondCmd.wait(lock, [=] { return !_gblRunning || (_gCmd & Mask) == Val; });
	return _gblRunning;
}

/* @brief Set command flag */
static void _SetCmdsState(int cmds, bool blOn)
{
	unique_lock<mutex> lock(_gMtxCmd);

	if (blOn) {
		_gCmd |= cmds;
	} else {
		_gCmd &= ~cmds;
	}

	_gCondCmd.notify_all();
}

/* @brief Set command flag on */
static inline void SetCmdsOn(int cmds)
{
	return _SetCmdsState(cmds, true);
}

/* @brief Set command flag off */
static inline void SetCmdsOff(int cmds)
{
	return _SetCmdsState(cmds, false);
}

/* @brief Wait command flag on */
static inline bool WaitCmdsOn(int cmds)
{
	return _WaitCmds(cmds, cmds);
}

/* @brief Wait command flag off */
static inline bool WaitCmdsOff(int cmds)
{
	return _WaitCmds(cmds, 0);
}

/* @brief Thread for quering NIR image on demand */
static void NirWorker()
{
	unsigned char *pImage = NULL;
	int ImageSize = 0;

	while (_gblRunning) {

#ifndef SHOW_IMG_WIN
		if (!WaitCmdsOn(CMD_REQ_NIR)) {
			break;
		}
#endif

		if (e_devicetype_etron_3d_camera == g_deviceType)
		{
			cv::Mat matImage;

			GetPreviewImage_ETron(matImage);

			pImage = matImage.data;
			ImageSize = matImage.total() * matImage.elemSize(); // matImage.cols * matImage.rows * matImage.depth() *matImage.channels();
			//1280 x 720 x 3 = 2764800
			//DSG(1, "NirWorker 4 matImage.cols=%d matImage.rows=%d matImage.elemSize()=%d",
			//	matImage.cols, matImage.rows, matImage.elemSize());
		#ifdef SHOW_IMG_WIN
			static int oneShotCnt = 0;
			if (60 == oneShotCnt)
			{
				SaveCameraFrame_ETron_PreviewBmp(matImage, "preview.bmp");
			}
			++oneShotCnt;
			//_gpCw->DrawCapture(0, 0, NIR_IMG_HEIGHT, NIR_IMG_WIDTH, 24, pImage);
			_gpCw->DrawCapture(0, 0, NIR_IMG_WIDTH, NIR_IMG_HEIGHT, 24, pImage);
		#else
			//_gpCw->DrawCapture(0, 0, NIR_IMG_WIDTH, NIR_IMG_HEIGHT, 24, pImage);
			//LogDebug("Output nir img of size [%d]\n", ImageSize);
			#ifdef USE_IPC_ERROR
				fwrite(pImage, 1, NIR_IMG_SIZE, stderr);
				fflush(stderr);
			#else
				size_t size = fwrite(pImage, 1, NIR_IMG_SIZE, stdout);
				//DSG(1, "depth NIR_IMG_SIZE=%u size=%u", NIR_IMG_SIZE, size);
				fflush(stdout);
			#endif

			SetCmdsOff(CMD_REQ_NIR);
		#endif
		}
		else if (e_devicetype_hmx_3d_camera == g_deviceType)
		{
			//if (ret_FAIL == camera_query_image(RGB_CAM, &pImage, &ImageSize)) {
			//	Log("Failed to query nir image\n");
			//	continue;
			//}
		#ifdef SHOW_IMG_WIN
			_gpCw->DrawCapture(0, 0, NIR_IMG_HEIGHT, NIR_IMG_WIDTH, 24, pImage);
		#else
			LogDebug("Output nir img of size [%d]\n", ImageSize);
			fwrite(pImage, 1, NIR_IMG_SIZE, stdout);
			fflush(stdout);
			SetCmdsOff(CMD_REQ_NIR);
		#endif
		}

	}

	Log("Exit nir thread\n");
}

/* @brief Thread for quering depth image on demand */
static void DepWorker()
{
	unsigned char *pImage = NULL;
	int ImageSize = 0;
	
	while (_gblRunning) {

#ifndef SHOW_IMG_WIN
		if (!WaitCmdsOn(CMD_REQ_DEP)) {
			break;
		}
#endif
		if (e_devicetype_etron_3d_camera == g_deviceType)
		{
			cv::Mat matImage;

			GetDepthImage_ETron(matImage);
			pImage = matImage.data;
			ImageSize = matImage.total() * matImage.elemSize(); // matImage.cols * matImage.rows * matImage.depth() *matImage.channels();
			//1280, 720, 2
			//DSG(1, "DepWorker 3 matImage.cols=%d matImage.rows=%d matImage.elemSize()=%d", 
			//	matImage.cols, matImage.rows, matImage.elemSize());

			/* Wait for writing NIR image complete */
			if (!WaitCmdsOff(CMD_REQ_NIR)) {
				break;
			}

		#ifdef SHOW_IMG_WIN
			static int oneShotCnt = 0;
			if (60 == oneShotCnt)
			{
				SaveCameraFrame_ETron_DepthBmp(matImage, "depth.bmp");
				SaveCameraFrame_ETron_DepthRaw(matImage, "depth.raw");
			}
			++oneShotCnt;
		#else
			//LogDebug("Output depth img of size [%d]\n", DEP_IMG_SIZE);
			#ifdef USE_IPC_ERROR
				fwrite(pImage, 1, DEP_IMG_SIZE, stderr);
				fflush(stderr);
			#else
				size_t size = fwrite(pImage, 1, DEP_IMG_SIZE, stdout);
				//DSG(1, "depth DEP_IMG_SIZE=%u size=%u", DEP_IMG_SIZE, size);
				fflush(stdout);
			#endif
			SetCmdsOff(CMD_REQ_DEP);
		#endif
		}
		else if (e_devicetype_hmx_3d_camera == g_deviceType)
		{
			//if(ret_FAIL == camera_query_image(DEPTH_CAM,&pImage,&ImageSize)) {
			//	Log("Failed to query depth image\n");
			//	continue;
			//}

			//for(int j = 0; j < DEP_IMG_ORIG_HEIGHT; j++) {
			//	for(int i = 0; i < DEP_IMG_ORIG_WIDTH; i++) {
			//		
			//		DepImg[i * DEP_IMG_HEIGHT *		 DEP_IMG_DEPTH + j * DEP_IMG_DEPTH + 1]
			//	  = pImage[i * DEP_IMG_ORIG_HEIGHT * DEP_IMG_DEPTH + j * DEP_IMG_DEPTH + 1];

			//		DepImg[i * DEP_IMG_HEIGHT *		 DEP_IMG_DEPTH + j * DEP_IMG_DEPTH]
			//	  = pImage[i * DEP_IMG_ORIG_HEIGHT * DEP_IMG_DEPTH + j * DEP_IMG_DEPTH];
			//	}
			//}			

			/* Wait for writing NIR image complete */
			if (!WaitCmdsOff(CMD_REQ_NIR)) {
				break;
			}

		#ifdef SHOW_IMG_WIN
			_gpCw->DrawCapture(800, 0, DEP_IMG_HEIGHT, DEP_IMG_WIDTH, 16, DepImg);
		#else
			LogDebug("Output depth img of size [%d]\n", DEP_IMG_SIZE);
			fwrite(DepImg, 1, DEP_IMG_SIZE, stdout);
			fflush(stdout);

			SetCmdsOff(CMD_REQ_DEP);
		#endif
		}
	}

	Log("Exit depth thread\n");
}

/* @brief Read commands from stdin */
static int GetCommds()
{
	char cmds = 0;

	if (1 != fscanf_s(stdin, "%c", &cmds, 1)) {
		Log("Failed to get cmds [%d], set to [quit]\n", cmds);
		cmds = CMD_QUIT;
	} else {
		LogDebug("Receive cmds [%d]\n", cmds);
	}

	return cmds;
}

/* ################
 * ##    Main    ##
 * ################ */
int main(int argc, char *argv[])
{
	OpenLog();
#ifdef USE_IPC_CMD_CTRL
#ifdef USE_IPC_ERROR
	if (-1 == _setmode(_fileno(stderr), _O_BINARY)) {
#else
	if (-1 == _setmode(_fileno(stdout), _O_BINARY)) {
#endif
		Log("Failed to change stdout to binary mode\n");
		return -1;
	}
#endif
	OpenCamera();
	StartCamera();

	//for(int j = 0; j < DEP_IMG_ORIG_HEIGHT; j++) {
	//	for(int i = 0; i < DEP_IMG_ORIG_WIDTH; i++) {
	//		DSG(1, "1 DepImg[%d * %d * %d + %d * %d + 1] = pImage[%d * %d * %d + %d * %d + 1] ",
	//			i,DEP_IMG_HEIGHT, DEP_IMG_DEPTH, j, DEP_IMG_DEPTH,
	//			i,DEP_IMG_ORIG_HEIGHT, DEP_IMG_DEPTH, j, DEP_IMG_DEPTH);
	//		DSG(1, "2 DepImg[%d * %d * %d + %d * %d] = pImage[%d * %d * %d + %d * %d] ",
	//			i, DEP_IMG_HEIGHT, DEP_IMG_DEPTH, j, DEP_IMG_DEPTH,
	//			i, DEP_IMG_ORIG_HEIGHT, DEP_IMG_DEPTH, j, DEP_IMG_DEPTH);
	//	}
	//}

	thread NirThread = thread(NirWorker);
	thread DepThread = thread(DepWorker);

#ifdef SHOW_IMG_WIN
	_gpCw = new CaptureWindow(g_deviceType);
	_gpCw->Show();
	_gblRunning = false;
#else
	//_gpCw = new CaptureWindow(g_deviceType);
	//_gpCw->Show();

	char ch;
	while (_gblRunning) {
	#ifdef USE_IPC_CMD_CTRL
		//DSG(1, "_gblRunning 2 =%d", _gblRunning);
		switch (GetCommds()) {
		case CMD_REQ_IMG:
			//DSG(1, "CMD_REQ_IMG");
			SetCmdsOn(CMD_REQ_IMG);
			/* WaitCmdsOff(CMD_REQ_IMG); */
			break;
		case CMD_QUIT:
			DSG(1, "CMD_QUIT");
			_gblRunning = false;
			SetCmdsOn(CMD_QUIT);
		}
	#else
		Sleep(50);
		ch = getch();
		if (ch != 'q')
		{
			DSG(1, "ch %c", ch);
		}
		else if (ch == '1')
		{
			SetCmdsOn(CMD_REQ_IMG);
		}
		else
		{
			_gblRunning = false;
			SetCmdsOn(CMD_QUIT);
			DSG(1, "ch qq");
		}
	#endif
	} // while
#endif

	NirThread.join();
	DepThread.join();

	StopCamera();
	CloseCamera();

	Log("Exit process\n");

	return 0;
}
