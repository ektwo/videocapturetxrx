#ifndef HMXLIB_H
#define HMXLIB_H

 #define __EXTERN_C  extern "C"

 #define HMXLIB_API __declspec(dllexport)
 #define HMXLIB_C_API __EXTERN_C __declspec(dllexport)

typedef enum
{
	DEPTH_CAM = 0,  //Deapth
	RGB_CAM = 1, // RBG
	UNKNOW_CAM=0xFF,
}CameraType_t;

typedef enum
{
	ret_PASS = 0,
	ret_FAIL = -1,
	ret_NoCam = -2,
	ret_UnSupportResolution = -3,
	ret_NoImageBuffer = -4,
	ret_ErrParameter = -5,
}ErrType_t;

typedef enum
{
	GPIO_XRES = 0,    /**< Image Sensor Reset IO (XRES)         */
	GPIO_XSHUTDOWN,   /**< Image Sensor Shutdown IO (XSHUTDOWN) */
	GPIO_RST,		 /**< Cypress RST IO                       */
	GPIO_MAX
}GPIO_t;

typedef enum
{
	DEPTH_STATE_3D,
	DEPTH_STATE_2D,
}depth_state_t;

typedef enum
{
	FLASH_RGB_PROG,			/**< Flash Section: RGB Program/Firmware **/
	FLASH_RGB_SEN,			/**< Flash Section: RGB Sensor Setting **/
	FLASH_DEPTH,			/**< Flash Section: Depth Program/Firmware **/
	FLASH_RICA,
	FLASH_MAX
}FlashIndex_t;

typedef enum
{
	ADDRESS_1BYTE = 1,
	ADDRESS_2BYTES = 2,
	ADDRESS_MAXIMUM
} RegAddressByte_t;

// initial lib
HMXLIB_C_API ErrType_t init_lib();

//de init lib
HMXLIB_C_API ErrType_t deinit_lib();

//open camera
//when RGB camera, it support several resolution
//when Depth camera, it should be only one kind of resolution
HMXLIB_C_API ErrType_t camera_open(CameraType_t camera_type, int cam_height, int cam_width);


//close camera(camera type)
HMXLIB_C_API ErrType_t camera_close(CameraType_t camera_type);

/**
 [Depth image]
 - resolution :  123(width) * 215(height)
 - data format : 2 bytes in each pixel
   ----------------------------------------------------------------------------------------------------------
   |  pixel 0 low byte  | pixel 0 high byte  | pixel 1 low byte  | pixel 1 high byte  | ...
   ----------------------------------------------------------------------------------------------------------

 [distance]
 - depth value(2bytes)/8 = distance(mm).
 for example depth value 0x1F83 = 8067/8 = 1,008mm

 [RGB image]
- resolution :  720(width) * 1280(height)
 - data format : 3 bytes in each pixel
   ---------------------------------------------------------------------------------------------------------------------------
   |  pixel 0 R byte  | pixel 0 G byte  |  pixel B byte  |  pixel 1 R byte  | pixel 1 G byte  |  pixel 1 B byte  |...
   ---------------------------------------------------------------------------------------------------------------------------

**/

HMXLIB_C_API ErrType_t camera_query_image(CameraType_t camera_type, unsigned char **image_data, signed __int32 *data_size);

ErrType_t camera_frame_sync();


//extension control

// i2c read with address 1byte/2bytes
HMXLIB_C_API ErrType_t i2c_read(CameraType_t camera_type, unsigned char dev_id, unsigned short address, RegAddressByte_t address_byte, unsigned char *data, unsigned short data_length, bool burst);

// i2c write with address 1byte/2bytes
HMXLIB_C_API ErrType_t i2c_write(CameraType_t camera_type, unsigned char dev_id, unsigned short address, RegAddressByte_t address_byte, unsigned char *data, unsigned short data_length, bool burst);


HMXLIB_C_API ErrType_t program_Flash(FlashIndex_t flash_type, char *fileName);

HMXLIB_C_API ErrType_t state_change(depth_state_t state);

ErrType_t gpio_control(CameraType_t camera_type, GPIO_t gpio, bool value);

//set_CyDevRst_dev high then low
ErrType_t camera_reset(CameraType_t camera_type);



#endif
