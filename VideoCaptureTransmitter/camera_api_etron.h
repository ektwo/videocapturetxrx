#ifndef __CAMERA_WRAPPER_H__
#define __CAMERA_WRAPPER_H__

#pragma once


#include <opencv2/opencv.hpp>

int OpenCamera_ETron(int deviceIdx);
void CloseCamera_ETron();
void StartCamera_ETron();
void StopCamera_ETron();
void SaveCameraFrame_ETron_PreviewBmp(const cv::Mat &matSrc, const char *filename);
void SaveCameraFrame_ETron_DepthBmp(const cv::Mat &matSrc, const char *filename);
void SaveCameraFrame_ETron_DepthRaw(const cv::Mat &matSrc, const char *filename);
void GetPreviewImage_ETron(cv::Mat &matImage);
void GetDepthImage_ETron(cv::Mat &matImage);

#endif