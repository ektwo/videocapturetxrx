#ifndef DEPTHCAMERA_H
#define DEPTHCAMERA_H

#include <vector>
#include <mutex>
#include <queue>
#include <opencv2/opencv.hpp>
#include <windows.h>
#include "../common/common.h"
#include "../common/kneron_face3dgrabber.h"
#include "../common/abstractcamera.h"
#include "eSPDI\\DM\\include\\eSPDI_DM.h"


typedef enum {
	TRANSFER_TO_COLORFULRGB,
	TRANSFER_TO_GRAYRGB
}DEPTHMAP_TYPE;

struct myMat
{
	cv::Mat img; /// Standard cv::Mat

	myMat() {};   /// Default constructor
	~myMat() {};  /// Destructor (called by queue::pop)

				  /// Copy constructor (called by queue::push)
	myMat(const myMat& src)
	{
		src.img.copyTo(img);
	}
};

class CPreviewItem
{
public:
	CPreviewItem();
	~CPreviewItem();

	void Reset();
	cv::Rect m_imgRes;
};

class CPreviewParams
{
public:
	CPreviewParams();
	~CPreviewParams();

    void Reset();

    CCritSec m_mutex;
	CPreviewItem m_colorPreview;
	int m_colorOption;
	bool m_360ModeEnabled;
	BOOL m_isImgRGB;
	BYTE m_ctrlMode;
	CPreviewItem m_depthPreview[ETronDI_MAX_DEPTH_STREAM_COUNT];
	std::vector<unsigned char> m_depthFusionSelectedIndex;
	bool m_showFusionSelectedDlg;
	std::vector<unsigned char> m_fusionTargetRgbImgBuf;
	int m_depthOption;
	int m_depthSwitch;
	bool m_IsDepthFusionOn;
	bool m_postProcess;
	WORD m_depthType;				//bits of depth data
	DEPTHMAP_TYPE m_depthMapType;	//draw depth in colorful or gray scale 
	WORD m_nDepthColorMapMode;		//draw depth data with selectable color mapping, for 11/14 bits, colorful type only.
	bool m_rectifyData;
	double m_camFocus;
	std::vector<double> m_baselineDist;

};

class DepthCamera : public AbstractCamera
{
#ifdef QT_STYLE
	Q_OBJECT
#endif
public:
	unsigned char m_ColorPalette[256][4];
	unsigned char m_GrayPalette[256][4];
	RGBQUAD		m_ColorPaletteD11[2048];
	RGBQUAD		m_GrayPaletteD11[2048];
	RGBQUAD		m_ColorPaletteZ14[16384];
	RGBQUAD		m_GrayPaletteZ14[16384];

public:
#ifdef QT_STYLE
	DepthCamera(QObject *parent = {});
#else
	DepthCamera();
#endif
	~DepthCamera();

    bool SetConfig(SCameraConfig *pConfig);
	bool Open();
	void Close();
	bool GetFirstFrame(cv::Mat &frame);

	static void ConvertToRaw(void *pUserData, cv::Mat &matSrc, uchar *lpData);

	unsigned short GetDevType() const { return m_DevType; }
	EtronDIImageType::Value GetDepthImageType() { return m_depthImageType; }
	DEPTHMAP_TYPE GetDepthMapType() { return m_DepthMapType; }
	unsigned char* GetImageBuffer() { return m_pImageBuf; }

	int GetColorStreamOptionCount() { return m_colorStreamOptionCount; }
	ETRONDI_STREAM_INFO* GetStreamColorInfos() { return &m_pStreamColorInfo[0]; }
	int GetDepthStreamOptionCount() { return m_depthStreamOptionCount; }
	ETRONDI_STREAM_INFO* GetStreamDepthInfos() { return &m_pStreamDepthInfo[0]; }

	void GetPreviewImage(cv::Mat &matImage);
	void GetDepthImage(cv::Mat &matImage);

	bool IsMultibaselineDevice();

	void ApplyImagePreview(unsigned char *pDataBuf, int dataSize, BOOL bIsOutputRGB, BOOL bIsMJPEG, int nColorSerialNum);
    void ApplyImageCapture(unsigned char *pDataBuf, int nDepthSerialNum);
	static void ImgCallback(EtronDIImageType::Value imgType, int imgId, unsigned char *imgBuf, int imgSize,int width, int height, int serialNumber, void* pParam);

#ifdef QT_STYLE
signals:
#endif
	void started();
	void stopped();
	void matColorReady(const cv::Mat &);
	void matDepthReady(const cv::Mat &);

#ifdef QT_STYLE
public slots:
#endif
	void start(int cam = {});
	void stop();


private:
	bool Start();
	void Stop();

	bool FindDevices();
	void SelectDevice(int deviceIdx);
	void SetIRValue(WORD value);

	void GetHWInfo();

	//CPreviewDlg::CPreviewDlg
	void InitParams();

	//CPreviewImageDlg::OnBnClickedPreviewBtn
	bool InitHardware();
	void DeinitHardware();

		//PreparePreviewDlg(); //assign target
		bool InitHardwareInternal();

			void ResetDepthParams();

			bool AllocDepthParams();
			void FreeDepthParams();


	bool GetEtronDIDevice(std::vector<DEVINFORMATIONEX>& device);
	int  GetDepthStreamIndex(cv::Rect depthRes) const;
	void DepthTypeToUIOption(WORD type, bool& rectify, int& depthComboIndex) const;
	WORD UIOptionToDepthType(bool rectify, int depthComboIndex);


	void UpdatePreviewParams();
	bool UpdateStreamInfo();
	void UpdateIRConfig();
	void UpdateUI();
	
	cv::Rect CurColorImgRes();
	cv::Rect CurDepthImgRes(int depthIndex);
	int EnableDenoise(bool enable);
	int InitEysov(LONG& outWidth, LONG& outHeight);

#ifdef QT_VERSION
private:
	void timerEvent(QTimerEvent * ev);
	int m_timerId;
#endif

private:
	EComponentState m_state;
	// whole
	void *m_hEtronDI;
	DEVSELINFO m_DevSelInfo;
	unsigned short m_DevType;
	std::vector<DEVINFORMATIONEX> m_devInfo;

	// preview
	int m_colorStreamOptionCount;
	ETRONDI_STREAM_INFO m_pStreamColorInfo[ETronDI_MAX_STREAM_COUNT];
	int m_depthStreamOptionCount;
	ETRONDI_STREAM_INFO m_pStreamDepthInfo[ETronDI_MAX_STREAM_COUNT];
	std::pair<WORD, WORD> m_irRange;
	CPreviewParams m_previewParams;

	// preview
	bool m_bCurrentFrameIsOutputRGB;
	bool m_bCurrentFrameIsMJPEG;

	// depth
	int m_nDepthResWidth;
	int m_nDepthResHeight;
	int m_depthId;
	int m_zdTableIndex;
	int m_imgSerialNumber;
	int m_depthSerialNumber;
	WORD m_nDepthColorMapMode;
	double m_camFocus;
	double m_baselineDist;
	DEPTHMAP_TYPE m_DepthMapType;
	EtronDIImageType::Value m_depthImageType;
	unsigned char *m_pImageBuf;

	std::vector<unsigned char> m_imgBuf;
	std::vector<unsigned char> m_depthData;

#ifdef QT_VERSION
	QMutex m_mutexPreview;
	QMutex m_mutexCapture;
#else
	std::mutex m_mutexPreview;
	std::mutex m_mutexCapture;
	std::mutex m_mutexStart;

	std::condition_variable m_cond;
#endif
	ThreadSafeQueue<cv::Mat> m_imgPreviewList;
	ThreadSafeQueue<cv::Mat> m_imgDepthList;
	
	//std::queue<cv::Mat> m_imgPreviewList;
	//std::queue<cv::Mat> m_imgDepthList;
	//std::queue<myMat> m_imgPreviewList;
	//std::queue<myMat> m_imgDepthList;
	
	//11 bits
	//std::vector<double> m_mblDistMultiplier;
	//std::vector<unsigned char> m_tempBuf;
	union {
		BITMAPINFO	m_bmiDepth;
		BYTE		m_bmiBYTEDepth[sizeof(BITMAPINFOHEADER) + sizeof(RGBQUAD) * 256];
	};

public: void Run()
	{
		//std::stringstream ss;
		//ss << std::this_thread::get_id();
		//uint64_t id = std::stoull(ss.str());
		//DSG(1, "Run id=%llu", id);
		{
			std::unique_lock<std::mutex> lock(m_mutexStart);
			m_cond.wait(lock, [=] { return (e_component_state_started == m_state); });
		}
		while (e_component_state_started == m_state)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(1000));
		}
	}
};

#endif // DEPTHCAMERA_H
