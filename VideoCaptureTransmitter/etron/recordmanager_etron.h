#ifndef __RECORDMANAGER_ETRON_H__
#define __RECORDMANAGER_ETRON_H__

#pragma once

void RecordingManager_ETron_SaveToBmp(void *pUserData, const cv::Mat &matSrc, const char *filename);
void RecordingManager_ETron_ConvertToBmp24Bits(void *pUserData, const cv::Mat &matSrc, const char *filename);
void RecordingManager_ETron_ConvertToRaw(void *pUserData, const cv::Mat &matSrc, const char *filename);

#endif
