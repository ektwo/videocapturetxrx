#include "math.h"
#include "etronapi.h"



void YUY2_to_RGB24_Convert(BYTE *pSrc, BYTE *pDst, int cx, int cy)
{
    int nSrcBPS,nDstBPS,x,y,x2,x3,m;
    int ma0,mb0,m02,m11,m12,m21;
    BYTE *pS0,*pD0;
    int Y,U,V,Y2;
    BYTE R,G,B,R2,G2,B2;
    //
    nSrcBPS = cx*2;
    nDstBPS = ((cx*3+3)/4)*4;
    //
    pS0 = pSrc;
    pD0 = pDst+nDstBPS*(cy-1);
    for (y=0; y<cy; y++) {
        for (x3=0,x2=0,x=0; x<cx; x+=2,x2+=4,x3+=6) {
            Y = (int)pS0[x2+0]-16;
            Y2= (int)pS0[x2+2]-16;
            U = (int)pS0[x2+1]-128;
            V = (int)pS0[x2+3]-128;
            //
            ma0 = 298*Y;
            mb0 = 298*Y2;
            m02 =  409*V+128;
            m11 = -100*U;
            m12 = -208*V+128;
            m21 =  516*U+128;
            //
            m = (ma0    +m02)>>8;
            R = (m<0)?(0):(m>255)?(255):((BYTE)m);
            m = (ma0+m11+m12)>>8;
            G = (m<0)?(0):(m>255)?(255):((BYTE)m);
            m = (ma0+m21    )>>8;
            B = (m<0)?(0):(m>255)?(255):((BYTE)m);
            //
            m = (mb0    +m02)>>8;
            R2 = (m<0)?(0):(m>255)?(255):((BYTE)m);
            m = (mb0+m11+m12)>>8;
            G2 = (m<0)?(0):(m>255)?(255):((BYTE)m);
            m = (mb0    +m21)>>8;
            B2 = (m<0)?(0):(m>255)?(255):((BYTE)m);
            //
            pD0[x3  ] = B;
            pD0[x3+1] = G;
            pD0[x3+2] = R;
            pD0[x3+3] = B2;
            pD0[x3+4] = G2;
            pD0[x3+5] = R2;
        }
        pS0 += nSrcBPS;
        pD0 -= nDstBPS;
    }
}

void HSV_to_RGB(double H, double S, double V, double &R, double &G, double &B)
{
    double nMax,nMin;
    double fDet;
    //
    while (H<0.0) H+=360.0;
    while (H>=360.0) H-=360.0;
    H /= 60.0;
    if (V<0.0) V = 0.0;
    if (V>1.0) V = 1.0;
    V *= 255.0;
    if (S<0.0) S = 0.0;
    if (S>1.0) S = 1.0;
    //
    if (V == 0.0) {
        R = G = B = 0;
    } else {
        fDet = S*V;
        nMax = (V);
        nMin = (V-fDet);
        if (H<=1.0) { //R>=G>=B, H=(G-B)/fDet
            R = nMax;
            B = nMin;
            G = (H*fDet+B);
        } else if (H<=2.0) { //G>=R>=B, H=2+(B-R)/fDet
            G = nMax;
            B = nMin;
            R = ((2.0-H)*fDet+B);
        } else if (H<=3.0) { //G>=B>=R, H=2+(B-R)/fDet
            G = nMax;
            R = nMin;
            B = ((H-2.0)*fDet+R);
        } else if (H<=4.0) { //B>=G>=R, H=4+(R-G)/fDet
            B = nMax;
            R = nMin;
            G = ((4.0-H)*fDet+R);
        } else if (H<=5.0) { //B>=R>=G, H=4+(R-G)/fDet
            B = nMax;
            G = nMin;
            R = ((H-4.0)*fDet+G);
        } else { // if(H<6.0) //R>=B>=G, H=(G-B)/fDet+6
            R = nMax;
            G = nMin;
            B = ((6.0-H)*fDet+G);
        }
    }
}

void ColorMap(double k, double& R, double& G, double& B)
{
    double r;

    if (k < 0.0) k = 0.0;
    if (k > 1.0) k = 1.0;
    if (k < 0.1) {
        r = k / 0.1;
        R = G = B = 128.0 + r * 127.0; //128~255
    }
    else if (k < 0.2) {
        k -= .1;
        r = k / 0.1;
        R = 255.0;
        G = B = (1.0 - r) * 255.0;//255~0
    }
    else if (k < 0.35) {
        k -= .2;
        r = k / 0.15;
        B = 0.0; //B
        G = r * 255.0; // 0~255
        R = 255.0; //R
    }
    else if (k < 0.5) {
        k -= 0.35;
        r = k / 0.15;
        B = 0.0;
        G = (1.0 - r / 4.0) * 255.0;  //255~196
        R = (1.0 - r / 2.0) * 255.0; //255~128
    }
    else if (k < 0.6) {
        k -= 0.5;
        r = k / 0.1;
        B = r * 128.0; //B 0~128
        G = 196.0; //Gc
        R = (1.0 - r) * 128.0; //R 128~0
    }
    else if (k < 0.7) {
        k -= 0.6;
        r = k / 0.1;
        B = 128.0 + r * 127.0; //B 128~255
        G = 196.0; //G
        R = 0.0; //R
    }
    else if (k < 0.8) {
        k -= 0.7;
        r = k / 0.1;
        B = 255; //B
        G = (1.0 - r) * 196.0; //G 196~0
        R = 0; //R
    }
    else if (k < 0.9) {
        k -= 0.8;
        r = k / 0.1;
        B = (1.0 - r / 2.0) * 255.0; //B 255~128
        G = 0.0; //G
        R = r * 128.0; //R=0~128
    }
    else {
        k -= .9;
        r = k / .1;
        R = B = (1 - r) * 128; //B 128~0
        G = 0; //G
    }
}

void DmColorMode(unsigned char pallete[256][4], int mode)
{
    int i;
    double R, G, B;

    if (mode == 0) {
        for (i = 0; i<256; i++) {
            HSV_to_RGB((255-i)*360.0/(double)255.0,1.0,1.0,R,G,B);

            pallete[i][0] = (BYTE)B;
            pallete[i][1] = (BYTE)G;
            pallete[i][2] = (BYTE)R;
            pallete[i][3] = 0;
        }

    }
    else if (mode == 1) { //double color scale
        for (i = 0; i<255; i++) {
            HSV_to_RGB(((255 - i)>>1<<1)*360.0 / (double)255, (i % 2) ? 0.8 : 1.0, 1.0, R, G, B);

            pallete[i][0] = (BYTE)B;
            pallete[i][1] = (BYTE)G;
            pallete[i][2] = (BYTE)R;
            pallete[i][3] = 0;
        }

    }
    else if (mode == 2) { //near color
        for (i = 0; i<128; i++) {
            HSV_to_RGB((127 - i)*360.0 / (double)127.0, 1.0, 1.0, R, G, B);

            pallete[i][0] = (BYTE)B;
            pallete[i][1] = (BYTE)G;
            pallete[i][2] = (BYTE)R;
            pallete[i][3] = 0;
        }
        for (i = 128; i < 256; i++) {
            B = (i - 128 ) * 2;
            pallete[i][0] = (BYTE)B;
            pallete[i][1] = (BYTE)B;
            pallete[i][2] = (BYTE)B;
            pallete[i][3] = 0;

        }

    }
    else if (mode == 3) { //far color
        for (i = 0; i<128; i++) {
            B = i * 2;
            pallete[i][0] = (BYTE)B;
            pallete[i][1] = (BYTE)B;
            pallete[i][2] = (BYTE)B;
            pallete[i][3] = 0;

        }
        for (i = 128; i < 256; i++) {
            HSV_to_RGB((256 - i)*360.0 / (double)128.0, 1.0, 1.0, R, G, B);

            pallete[i][0] = (BYTE)B;
            pallete[i][1] = (BYTE)G;
            pallete[i][2] = (BYTE)R;
            pallete[i][3] = 0;

        }

    }

}

#ifdef ENABLE_LONG_DEPTHCOLOR_MAP
void DmColorMode11(RGBQUAD pallete[2048], int mode)
{
#define CP1	0.75
#define CP2 0.25

    int i;
    double R, G, B;
    int t1, t2; //focus region, 0.25~0.75 mapping area
    switch (mode) {
    case 1: //near
        t1 = 512;
        t2 = 1024;
        break;
    case 2: //midle
        t1 = 200;
        t2 = 512;
        break;
    case 3: //far
        t1 = 5;
        t2 = 256;
        break;
    default: //normal
        t1 = 256;
        t2 = 512;
        break;
    }
    double m, b; //y=mx+b
    //0~t1
    m = (CP1 - 1.0) / (double)t1;
    b = 1.0;
    for (i = 0; i<t1; i++) {
        ColorMap(m* (double) i + b, R, G, B);
        pallete[i].rgbBlue = (BYTE)B;
        pallete[i].rgbGreen = (BYTE)G;
        pallete[i].rgbRed = (BYTE)R;
        pallete[i].rgbReserved = 0;
    }
    m = (CP2 - CP1) / (double)(t2-t1);
    b = CP1 - m*(double)t1;
    for (; i<t2; i++) {
        ColorMap(m* (double)i + b, R, G, B);
        pallete[i].rgbBlue = (BYTE)B;
        pallete[i].rgbGreen = (BYTE)G;
        pallete[i].rgbRed = (BYTE)R;
        pallete[i].rgbReserved = 0;
    }
    m = (0 - CP2) / (double)(2048 - t2);
    b = CP2 - m*(double)t2;
    for (; i<2048; i++) {
        ColorMap(m* (double)i + b, R, G, B);
        pallete[i].rgbBlue = (BYTE)B;
        pallete[i].rgbGreen = (BYTE)G;
        pallete[i].rgbRed = (BYTE)R;
        pallete[i].rgbReserved = 0;
    }


}
void DmColorMode14(RGBQUAD *pallete, int mode)
{
#define CP1	0.75
#define CP2 0.25
    int length = 16384;
    int i;
    double R, G, B;
    int t1, t2; //focus region, 0.25~0.75 mapping area
    switch (mode) {
    case 1: //near
        t1 = 512*8;
        t2 = 1024 * 8;
        break;
    case 2: //midle
        t1 = 200 * 8;
        t2 = 512 * 8;
        break;
    case 3: //far
        t1 = 5 * 8;
        t2 = 256 * 8;
        break;
    default: //normal
        t1 = 256 * 8;
        t2 = 512 * 8;
        break;
    }
    double m, b; //y=mx+b
                 //0~t1
    m = (CP1 - 1.0) / (double)t1;
    b = 1.0;
    for (i = 0; i<t1; i++) {
        ColorMap(m* (double)i + b, R, G, B);
        pallete[i].rgbBlue = (BYTE)B;
        pallete[i].rgbGreen = (BYTE)G;
        pallete[i].rgbRed = (BYTE)R;
        pallete[i].rgbReserved = 0;
    }
    m = (CP2 - CP1) / (double)(t2 - t1);
    b = CP1 - m*(double)t1;
    for (; i<t2; i++) {
        ColorMap(m* (double)i + b, R, G, B);
        pallete[i].rgbBlue = (BYTE)B;
        pallete[i].rgbGreen = (BYTE)G;
        pallete[i].rgbRed = (BYTE)R;
        pallete[i].rgbReserved = 0;
    }
    m = (0 - CP2) / (double)(2048 - t2);
    b = CP2 - m*(double)t2;
    for (; i<length; i++) {
        ColorMap(m* (double)i + b, R, G, B);
        pallete[i].rgbBlue = (BYTE)B;
        pallete[i].rgbGreen = (BYTE)G;
        pallete[i].rgbRed = (BYTE)R;
        pallete[i].rgbReserved = 0;
    }

}
#else//not ENABLE_LONG_DEPTHCOLOR_MAP
void DmColorMode11(RGBQUAD pallete[2048], int mode)
{
    double R, G, B;
    for (int i = 0; i<2048; i++) {
        HSV_to_RGB((double)(i >> 3), 1.0, 1.0, R, G, B);

        pallete[i].rgbBlue = (BYTE)B;
        pallete[i].rgbGreen = (BYTE)G;
        pallete[i].rgbRed = (BYTE)R;
        pallete[i].rgbReserved = 0;
    }
}
void DmColorMode14(RGBQUAD *pColorPaletteZ14, int mode)
{
    int i;
    double R, G, B;
    double fx, fy;
    //
    double fCV = 180;
    int nCenter = 1500;
    double r1 = 0.35;
    double r2 = 0.55;
    //
    for (i = 1; i<16384; i++) {
        if (i == nCenter) {
            fy = fCV;
        }
        else if (i<nCenter) {
            fx = (double)(nCenter - i) / nCenter;
            fy = fCV - pow(fx, r1)*fCV;
        }
        else {
            fx = (double)(i - nCenter) / (16384 - nCenter);
            fy = fCV + pow(fx, r2)*(256 - fCV);
        }
        HSV_to_RGB(fy, 1.0, 1.0, R, G, B);
        pColorPaletteZ14[i].rgbBlue = (BYTE)B;
        pColorPaletteZ14[i].rgbGreen = (BYTE)G;
        pColorPaletteZ14[i].rgbRed = (BYTE)R;
        pColorPaletteZ14[i].rgbReserved = 0;
    }
    {
        i = 0;
        pColorPaletteZ14[i].rgbBlue = (BYTE)0;
        pColorPaletteZ14[i].rgbGreen = (BYTE)0;
        pColorPaletteZ14[i].rgbRed = (BYTE)0;
        pColorPaletteZ14[i].rgbReserved = 0;
    }
    {
        i = 16383;
        pColorPaletteZ14[i].rgbBlue = (BYTE)255;
        pColorPaletteZ14[i].rgbGreen = (BYTE)255;
        pColorPaletteZ14[i].rgbRed = (BYTE)255;
        pColorPaletteZ14[i].rgbReserved = 0;
    }
}
#endif //ENABLE_LONG_DEPTHCOLOR_MAP


void SetBaseColorPaletteD11(RGBQUAD *pColorPaletteD11)
{
    int i;
    double R,G,B;
    //
    for (i=0; i<2048; i++) {
        HSV_to_RGB((2047.0-i)/8,1.0,1.0,R,G,B);
        pColorPaletteD11[i].rgbBlue		= (BYTE)B;
        pColorPaletteD11[i].rgbGreen		= (BYTE)G;
        pColorPaletteD11[i].rgbRed		= (BYTE)R;
        pColorPaletteD11[i].rgbReserved	= 0;
    }
    {
        i = 0;
        pColorPaletteD11[i].rgbBlue		= (BYTE)0;
        pColorPaletteD11[i].rgbGreen		= (BYTE)0;
        pColorPaletteD11[i].rgbRed		= (BYTE)0;
        pColorPaletteD11[i].rgbReserved	= 0;
    }
    {
        i = 2047;
        pColorPaletteD11[i].rgbBlue		= (BYTE)255;
        pColorPaletteD11[i].rgbGreen		= (BYTE)255;
        pColorPaletteD11[i].rgbRed		= (BYTE)255;
        pColorPaletteD11[i].rgbReserved	= 0;
    }
}

void SetBaseGrayPaletteD11(RGBQUAD *pGrayPaletteD11)
{
    int i;
    double R,G,B;
    //
    for (i=0; i<2048; i++) {
        HSV_to_RGB((2047.0-i)/8,1.0,1.0,R,G,B);
        pGrayPaletteD11[i].rgbBlue		= (BYTE)B;
        pGrayPaletteD11[i].rgbGreen		= (BYTE)B;
        pGrayPaletteD11[i].rgbRed			= (BYTE)B;
        pGrayPaletteD11[i].rgbReserved	= 0;
    }
    {
        i = 0;
        pGrayPaletteD11[i].rgbBlue		= (BYTE)0;
        pGrayPaletteD11[i].rgbGreen		= (BYTE)0;
        pGrayPaletteD11[i].rgbRed			= (BYTE)0;
        pGrayPaletteD11[i].rgbReserved	= 0;
    }
    {
        i = 2047;
        pGrayPaletteD11[i].rgbBlue		= (BYTE)255;
        pGrayPaletteD11[i].rgbGreen		= (BYTE)255;
        pGrayPaletteD11[i].rgbRed			= (BYTE)255;
        pGrayPaletteD11[i].rgbReserved	= 0;
    }
}

void SetBaseColorPaletteZ14(RGBQUAD *pColorPaletteZ14)
{
    int i;
    double R,G,B;
    double fx,fy;
    //
    double fCV = 180;
    int nCenter=1500;
    double r1=0.35;
    double r2=0.55;
    //
    for (i=1; i<16384; i++) {
        if (i==nCenter) {
            fy = fCV;
        } else if (i<nCenter) {
            fx = (double)(nCenter-i)/nCenter;
            fy = fCV - pow(fx, r1)*fCV;
        } else {
            fx = (double)(i-nCenter)/(16384-nCenter);
            fy = fCV + pow(fx, r2)*(256-fCV);
        }
        HSV_to_RGB(fy,1.0,1.0,R,G,B);
        pColorPaletteZ14[i].rgbBlue		= (BYTE)B;
        pColorPaletteZ14[i].rgbGreen		= (BYTE)G;
        pColorPaletteZ14[i].rgbRed		= (BYTE)R;
        pColorPaletteZ14[i].rgbReserved	= 0;
    }
    {
        i = 0;
        pColorPaletteZ14[i].rgbBlue		= (BYTE)0;
        pColorPaletteZ14[i].rgbGreen		= (BYTE)0;
        pColorPaletteZ14[i].rgbRed		= (BYTE)0;
        pColorPaletteZ14[i].rgbReserved	= 0;
    }
    {
        i = 16383;
        pColorPaletteZ14[i].rgbBlue		= (BYTE)255;
        pColorPaletteZ14[i].rgbGreen		= (BYTE)255;
        pColorPaletteZ14[i].rgbRed		= (BYTE)255;
        pColorPaletteZ14[i].rgbReserved	= 0;
    }
}

void SetBaseGrayPaletteZ14(RGBQUAD *pGrayPaletteZ14)
{
    int i;
    double R,G,B;
    double fx,fy;
    //
    double fCV = 180;
    int nCenter=1500;
    double r1=0.35;
    double r2=0.55;
    //
    for (i=1; i<16384; i++) {
        if (i==nCenter) {
            fy = fCV;
        } else if (i<nCenter) {
            fx = (double)(nCenter-i)/nCenter;
            fy = fCV - pow(fx, r1)*fCV;
        } else {
            fx = (double)(i-nCenter)/(16384-nCenter);
            fy = fCV + pow(fx, r2)*(256-fCV);
        }
        HSV_to_RGB(fy,1.0,1.0,R,G,B);
        pGrayPaletteZ14[i].rgbBlue		= (BYTE)B;
        pGrayPaletteZ14[i].rgbGreen		= (BYTE)B;
        pGrayPaletteZ14[i].rgbRed			= (BYTE)B;
        pGrayPaletteZ14[i].rgbReserved	= 0;
    }
    {
        i = 0;
        pGrayPaletteZ14[i].rgbBlue		= (BYTE)0;
        pGrayPaletteZ14[i].rgbGreen		= (BYTE)0;
        pGrayPaletteZ14[i].rgbRed			= (BYTE)0;
        pGrayPaletteZ14[i].rgbReserved	= 0;
    }
    {
        i = 16383;
        pGrayPaletteZ14[i].rgbBlue		= (BYTE)255;
        pGrayPaletteZ14[i].rgbGreen		= (BYTE)255;
        pGrayPaletteZ14[i].rgbRed			= (BYTE)255;
        pGrayPaletteZ14[i].rgbReserved	= 0;
    }
}

void UpdateD11DisplayImage_DIB24(RGBQUAD *pColorPaletteD11, BYTE *pSrc, BYTE *pDst, int cx, int cy)
{
    int x, y, stride;
    WORD *pSrcW2, *pSrcW2Ptr;
    BYTE *pDstFromLast, *pDstFromLastTmp;
    RGBQUAD *pClr;
    //
    if ((cx <= 0) || (cy <= 0)) return;
    //
	stride = ((cx * 3 + 3) / 4) * 4;

    pSrcW2 = (WORD*)pSrc;
    pDstFromLast = pDst + (cy - 1) * stride;
    for (y = 0; y < cy; y++) {
        pSrcW2Ptr = pSrcW2;
        pDstFromLastTmp = pDstFromLast;
        for (x = 0; x < cx; x++) {
            pClr = &(pColorPaletteD11[pSrcW2Ptr[x]]);
            pDstFromLastTmp[0] = pClr->rgbBlue; //B
            pDstFromLastTmp[1] = pClr->rgbGreen; //G
            pDstFromLastTmp[2] = pClr->rgbRed; //R
            pDstFromLastTmp += 3;
        }
        pSrcW2 += cx;
        pDstFromLast -= stride;
    }
}

void UpdateZ14DisplayImage_DIB24(RGBQUAD *pColorPaletteZ14, BYTE *pSrc, BYTE *pDst, int cx, int cy)
{
	int x, y, stride;
    WORD *pSrcW2, *pSrcW2Ptr;
    BYTE *pDstFromLast, *pDstFromLastTmp;
    RGBQUAD *pClr;
    //
    if ((cx <= 0) || (cy <= 0)) return;
    //
    stride = ((cx * 3 + 3) / 4) * 4;
    pSrcW2 = (WORD*)pSrc;
    pDstFromLast = pDst + (cy-1) * stride;
    for (y = 0; y < cy; y++) {
        pSrcW2Ptr = pSrcW2;
        pDstFromLastTmp = pDstFromLast;
        for (x = 0; x < cx; x++) {
            pClr = &(pColorPaletteZ14[pSrcW2Ptr[x]]);
            pDstFromLastTmp[0] = pClr->rgbBlue; //B
            pDstFromLastTmp[1] = pClr->rgbGreen; //G
            pDstFromLastTmp[2] = pClr->rgbRed; //R
            pDstFromLastTmp += 3;
        }
        pSrcW2 += cx;
        pDstFromLast -= stride;
    }
}

