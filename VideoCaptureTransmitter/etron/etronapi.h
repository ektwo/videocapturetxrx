#ifndef ETRONAPI_H
#define ETRONAPI_H

#include <windows.h>
#include "Gdiplus.h"
#define ENABLE_LONG_DEPTHCOLOR_MAP


void HSV_to_RGB(double H, double S, double V, double &R, double &G, double &B);
void ColorMap(double k, double& R, double& G, double& B);
void DmColorMode(unsigned char pallete[256][4], int mode);
void DmColorMode11(RGBQUAD pallete[2048], int mode);
void DmColorMode14(RGBQUAD *pallete, int mode);
// set color palette for D11 and Z14
void SetBaseColorPaletteD11(RGBQUAD *pColorPaletteD11);
void SetBaseGrayPaletteD11(RGBQUAD *pGrayPaletteD11);
void SetBaseColorPaletteZ14(RGBQUAD *pColorPaletteZ14);
void SetBaseGrayPaletteZ14(RGBQUAD *pGrayPaletteZ14);
void UpdateD11DisplayImage_DIB24(RGBQUAD *pColorPaletteD11, BYTE *pSrc, BYTE *pDst, int cx, int cy);
void UpdateZ14DisplayImage_DIB24(RGBQUAD *pColorPaletteZ14, BYTE *pSrc, BYTE *pDst, int cx, int cy);

#endif // ETRONAPI_H
