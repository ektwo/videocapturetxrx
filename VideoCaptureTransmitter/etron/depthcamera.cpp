#include "depthcamera.h"
#ifdef QT_VERSION
#include <QDebug>
#include <QRect>
#include <QTimerEvent>
#endif
#ifdef OWNER_IS_KNERON_FACE3DGRABBER
#include "image_common.h"
#endif
#include "synchapi.h"
#include "../common/kneron_face3dgrabber.h"
#include "../common/common.h"
#include "etronapi.h"

#ifdef OWNER_IS_KNERON_FACE3DGRABBER
#else
KneronFace3DGrabber::GlobalCacheData m_globalCacheData;
KneronFace3DGrabber::GlobalCacheData *m_pGlobalCacheData = &m_globalCacheData;
#endif
bool gRectify = false;
int gDepthBitsComboIdx = -1;
//usb event 
void onDeviceEventFn(UINT pid, UINT vid, BOOL bAttached, void* pUserData)
{
	DepthCamera *pCamera = (DepthCamera*)pUserData;

	if (bAttached)
		DSG(1, "Device PID=0x%X, VID=0x%X attached.\nYou can call EtronDI_Init2 to initialize the SDK and call EtronDI_OpenEx2 to open the device.", pid, vid, bAttached);
	else
		DSG(1, "Device PID=0x%X, VID=0x%X dettached\nYou can call EtronDI_Release to release resource allocated by the SDK.", pid, vid, bAttached);
}

CPreviewItem::CPreviewItem()
{
}

CPreviewItem::~CPreviewItem()
{
	Reset();
}

void CPreviewItem::Reset()
{

}

CPreviewParams::CPreviewParams()
{
    Reset();
}

CPreviewParams::~CPreviewParams()
{
    Reset();
}

void CPreviewParams::Reset()
{
    CAutoLock lock(&m_mutex);

    m_colorOption = -1;
    m_depthOption = -1;
    m_depthSwitch = 0;
    m_IsDepthFusionOn = false;
    m_rectifyData = true;
    m_360ModeEnabled = false;
    m_postProcess = false;
	m_depthType = ETronDI_DEPTH_DATA_8_BITS_RAW;// ETronDI_DEPTH_DATA_11_BITS;
    m_ctrlMode = 0x0;
    m_isImgRGB = FALSE;
    m_depthMapType = TRANSFER_TO_COLORFULRGB;
    m_nDepthColorMapMode = 0; //normal mapping mode
    m_camFocus = 0.0;
    m_baselineDist = std::vector<double>(ETronDI_MAX_DEPTH_STREAM_COUNT, 0.0);
    m_showFusionSelectedDlg = false;
}

#ifdef QT_VERSION
DepthCamera::DepthCamera(QObject *parent)
	: AbstractCamera(e_devicetype_camera_depth_etron, parent)
	, m_timerId(-1)
#else
DepthCamera::DepthCamera()
	: AbstractCamera(e_devicetype_camera_depth_etron)
#endif
	, m_state(e_component_state_idle)
	, m_bCurrentFrameIsOutputRGB(false)
	, m_bCurrentFrameIsMJPEG(false)
	, m_nDepthResWidth(0)
	, m_nDepthResHeight(0)
	, m_depthId(0)
	, m_zdTableIndex(0)
	, m_imgSerialNumber(0)
	, m_depthSerialNumber(0)
	, m_nDepthColorMapMode(0)
	, m_camFocus(0.0)
	, m_baselineDist(0.0)
	, m_DepthMapType(TRANSFER_TO_GRAYRGB)
	, m_depthImageType(EtronDIImageType::DEPTH_8BITS)
	, m_pImageBuf(nullptr)
{

}

DepthCamera::~DepthCamera()
{
	DSG(1, "DepthCamera::~DepthCamera()");
	Close();
}

bool DepthCamera::SetConfig(SCameraConfig *pConfig)
{
	bool ret = false;

	memcpy(&m_config, pConfig, sizeof(m_config));

	ret = FindDevices();
	if (ret)
	{
		SelectDevice(0);
		GetHWInfo();

		InitParams();
	}

	return ret;
}

bool DepthCamera::Open()
{
	if (e_component_state_idle == m_state)
	{
#ifndef IGN_SETTING
		EtronDI_SetSensorTypeName(m_hEtronDI, ETRONDI_SENSOR_TYPE_H22);
#if defined(GIVE_ME_MIRROR) && defined(USE_HW_MIRROR)
		EtronDI_EnableImageMirror(m_hEtronDI, &m_DevSelInfo, true);
#endif
#endif

		if (InitHardware())
		{
			DSG(1, "DepthCamera::Open() InitHardware O");
			m_state = e_component_state_initialized;
		}
		else
			DSG(1, "DepthCamera::Open() InitHardware X");
	}
	//std::stringstream ss;
	//ss << std::this_thread::get_id();
	//uint64_t id = std::stoull(ss.str());
	//DSG(1, "open id=%llu", id);
	return (e_component_state_initialized == m_state);
}

void DepthCamera::Close()
{
	DSG(1, "DepthCamera::Close()");

	Stop();

	m_previewParams.Reset();
	DeinitHardware();

	if (m_hEtronDI != nullptr)
	{
		EtronDI_Release(&m_hEtronDI);
		m_hEtronDI = nullptr;
	}
}

bool DepthCamera::GetFirstFrame(cv::Mat &frame)
{
	return true;
}

void DepthCamera::SetIRValue(WORD value)
{
	DSG(1, "DepthCamera::SetIRValue value=%d", value);
#ifndef IGN_SETTING
	if (value != m_irRange.first) // 0
	{
		EtronDI_SetIRMode(m_hEtronDI, &m_DevSelInfo, 0x03);// 2 bits on for opening both 2 ir
		EtronDI_SetCurrentIRValue(m_hEtronDI, &m_DevSelInfo, value);
	}
	else
	{
		EtronDI_SetCurrentIRValue(m_hEtronDI, &m_DevSelInfo, value);
		EtronDI_SetIRMode(m_hEtronDI, &m_DevSelInfo, 0x00);// turn off ir
	}
#endif
}

bool DepthCamera::Start()
{
	bool ret = false;

	DSG(1, "DepthCamera::Start");

	if ((e_component_state_initialized == m_state) || (e_component_state_stopped == m_state))
	{
		int fps = 1000 / m_config.interval_ms;// 30;// GetDlgItemInt(IDC_EDIT_FRAME_RATE);
		DSG(1, "DepthCamera::Start m_previewParams.m_colorOption=%d", m_previewParams.m_colorOption);
		DSG(1, "DepthCamera::Start m_previewParams.m_isImgRGB=%d", m_previewParams.m_isImgRGB);
		DSG(1, "DepthCamera::Start m_previewParams.m_depthOption=%d", m_previewParams.m_depthOption);
		DSG(1, "DepthCamera::Start m_previewParams.m_depthSwitch=%d", m_previewParams.m_depthSwitch);
		DSG(1, "DepthCamera::Start m_previewParams.m_ctrlMode=%d", m_previewParams.m_ctrlMode);
		DSG(1, "DepthCamera::Start fps=%d", fps);//30
		int iRet = EtronDI_OpenDeviceEx(m_hEtronDI, &m_DevSelInfo,
			m_previewParams.m_colorOption, m_previewParams.m_isImgRGB == TRUE,
			m_previewParams.m_depthOption, m_previewParams.m_depthSwitch, ImgCallback, this, &fps, m_previewParams.m_ctrlMode);

		DSG(1, "DepthCamera::Start iRet=%d", iRet);
		if (iRet == ETronDI_OK)
		{
			m_state = e_component_state_started;
			
		#ifdef QT_VERSION
			m_timerId = startTimer(30, Qt::CoarseTimer);
		#else
			{
				std::unique_lock<std::mutex> lock(m_mutexStart);
				m_cond.notify_all();
			}
		#endif

			ret = true;
		}
	}

	return true;
}

void DepthCamera::Stop()
{
	DSG(1, "DepthCamera::Stop <<<<< ");
	if (e_component_state_started == m_state)
	{
	#ifdef QT_VERSION
		if (-1 != m_timerId)
		{
			killTimer(m_timerId);
			m_timerId = -1;
		}
	#endif
		if (nullptr != m_hEtronDI)
			EtronDI_CloseDevice(m_hEtronDI, &m_DevSelInfo);

		m_state = e_component_state_stopped;
	}
	DSG(1, "DepthCamera::Stop >>>>> ");
}

bool DepthCamera::FindDevices()
{
	bool ret = false;

	bool enableSDKLog = false;
	//if (EtronDI_Init(&m_hEtronDI, enableSDKLog) < 0)
	//	return false;
	int count = 10;
	
	do {
		DSG(1, "DepthCamera::FindDevices count=%d", count);
		int numDevices = EtronDI_Init2(&m_hEtronDI, enableSDKLog, true);
		DSG(1, "DepthCamera::FindDevices numDevices=%d", numDevices);
		DSG(1, "DepthCamera::FindDevices m_hEtronDI=%p", m_hEtronDI);
		if ((numDevices >= 0) && (m_hEtronDI))
		{ 
			if (GetEtronDIDevice(m_devInfo))
			{

				EtronDI_RegisterDeviceEvents(m_hEtronDI, onDeviceEventFn, this);
				
				ret = true;
				break;
			}
			else
			{
				if (m_hEtronDI != NULL)
				{
					EtronDI_Release(&m_hEtronDI);
					m_hEtronDI = NULL;
				}
			}
		}
		--count;
		Sleep(200);
	} while (count >= 0);

	
	DSG(1, "m_hEtronDI=%p", m_hEtronDI);

	return ret;
}

void DepthCamera::SelectDevice(int deviceIdx)
{
	m_DevSelInfo.index = deviceIdx; // 0
	m_DevType = m_devInfo[deviceIdx].nDevType;
}

void DepthCamera::GetHWInfo()
{
	//GetFWVersion
	{
		std::string title;
		std::vector<char> szBuf(256, 0);
		int nActualLength = 0;

		EtronDI_GetFwVersion(m_hEtronDI, &m_DevSelInfo, &szBuf[0], 256, &nActualLength);
		std::string fwVer(&szBuf[0]);
		const size_t maxFwMsgLengthOnUI = 50;
		if (nActualLength > maxFwMsgLengthOnUI)
		{
			title += fwVer.substr(0, maxFwMsgLengthOnUI) + "\n";
			title += fwVer.substr(maxFwMsgLengthOnUI);
		}
		else
		{
			title = fwVer;
		}

		DSG(1, "GetFWVersion=%s", title.c_str());
	}

	//OnBnClickedGetPidvidBtn
	{
		// Get product ID and Vendor ID
		unsigned short nProductID = 0;
		unsigned short nVendorID = 0;
		char szBuf[256];

		if (EtronDI_GetPidVid(m_hEtronDI, &m_DevSelInfo, &nProductID, &nVendorID) != ETronDI_OK)
			DSG(1, "EtronDI_GetPidVid failed !!");
		else {

			sprintf_s(szBuf, 256, "%04x", nProductID);
			DSG(1, "nProductID=%s", szBuf);
			sprintf_s(szBuf, 256, "%04x", nVendorID);
			DSG(1, "nVendorID=%s", szBuf);
		}
	}
	//OnBnClickedGetSnBtn
	{
		BYTE szBuf[512] = { 0 };
		int nActualSNLenByByte = 0;

		if (EtronDI_GetSerialNumber(m_hEtronDI, &m_DevSelInfo, szBuf, 512, &nActualSNLenByByte) != ETronDI_OK) 
			DSG(1, "EtronDI_GetSerialNumber Failed !!");
		else {
			char *pStrSerialNumber = (char*)malloc(sizeof(char)*nActualSNLenByByte / 2 + 1);

			for (int i = 0; i<nActualSNLenByByte; i += 2) {

				pStrSerialNumber[i / 2] = szBuf[i];
			}

			pStrSerialNumber[nActualSNLenByByte / 2] = '\0';

			if (pStrSerialNumber) {

				delete pStrSerialNumber;
				pStrSerialNumber = NULL;
			}
		}
	}

}

void DepthCamera::InitParams()
{
	m_colorStreamOptionCount = 0;
	m_depthStreamOptionCount = 0;
	memset(m_pStreamColorInfo, 0, sizeof(ETRONDI_STREAM_INFO) * ETronDI_MAX_STREAM_COUNT);
	memset(m_pStreamDepthInfo, 0, sizeof(ETRONDI_STREAM_INFO) * ETronDI_MAX_STREAM_COUNT);

	UpdateStreamInfo();
	UpdateIRConfig();
	UpdateUI();
}

bool DepthCamera::InitHardware()
{
	bool ret = false;

	UpdatePreviewParams();

#ifndef IGN_SETTING
	EtronDI_ConfigCtrlMode(m_hEtronDI, &m_DevSelInfo, m_previewParams.m_ctrlMode);
#endif


#ifdef FOR_CAINI /* 11 -bit */
	if (m_DevType == PUMA && EtronDI_SetDepthDataType(m_hEtronDI, &m_DevSelInfo, 2) != ETronDI_OK)
	{
		DSG(1, ("EtronDI_SetDepthDataType Failed !!");
		return;
	}

#elif ESPDI_EG

#else
#ifndef IGN_SETTING
	DSG(1, "DepthCamera::SetConfig EtronDI_SetDepthDataType m_previewParams.m_depthType=%x", m_previewParams.m_depthType);
	if (m_DevType == PUMA && EtronDI_SetDepthDataType(m_hEtronDI, &m_DevSelInfo, m_previewParams.m_depthType) != ETronDI_OK)
	{
		DSG(1, "DepthCamera::SetConfig EtronDI_SetDepthDataType Failed !!");
		return false;
	}
#endif
#endif

	//PreparePreviewDlg();
	InitHardwareInternal();

#ifndef ESPDI_EG
	if (m_previewParams.m_IsDepthFusionOn)
	{
	//	//if (m_depthFusionHelper != nullptr)
	//	//{
	//	//	delete m_depthFusionHelper;
	//	//}

	//	QRect res = CurDepthImgRes(0);
	//	DSG(1, "res.x=%d\r\n", res.x);
	//	DSG(1, "res.y=%d\r\n", res.y);
	//	DSG(1, "m_previewParams.m_camFocus=%f\r\n", m_previewParams.m_camFocus);

	//	m_depthFusionHelper = new CDepthFusionHelper(3, res.x, res.y,
	//		m_previewParams.m_camFocus, m_previewParams.m_baselineDist, 30, CPreviewImageDlg::DepthFusionCallback, this);

	//	DSG(1, "m_previewParams.m_baselineDist.size=%d\r\n", m_previewParams.m_baselineDist.size());
	//	for (int i = 0; i < m_previewParams.m_baselineDist.size(); ++i)
	//		ErrMsg(_T("m_previewParams.m_baselineDist[%d]=%f\r\n"), i, m_previewParams.m_baselineDist[i]);
	//	DSG(1, "EtronDI_IsDeocclusionDevice(m_hEtronDI, &m_DevSelInfo)=%d\r\n", EtronDI_IsDeocclusionDevice(m_hEtronDI, &m_DevSelInfo));
	//	m_depthFusionHelper->SetFusionMethod(
	//		EtronDI_IsDeocclusionDevice(m_hEtronDI, &m_DevSelInfo) ? DepthMergeMethod::MBRbaseV0 : DepthMergeMethod::MBLbase);
	//	DSG(1, "((CButton*)GetDlgItem(IDC_CHECK_FUSION_SWPP))->GetCheck()=%d\r\n", ((CButton*)GetDlgItem(IDC_CHECK_FUSION_SWPP))->GetCheck());
	//	if (((CButton*)GetDlgItem(IDC_CHECK_FUSION_SWPP))->GetCheck() == BST_CHECKED)
	//	{
	//		m_depthFusionHelper->EnablePostProc(true);
	//	}
	}
#endif

#ifndef ESPDI_EG
	if (0)//GetDlgItem(IDC_CHECK_INTERLEAVE_MODE)->IsWindowEnabled())
	{
		EtronDI_EnableInterleave(m_hEtronDI, &m_DevSelInfo, 0);// ((CButton*)GetDlgItem(IDC_CHECK_INTERLEAVE_MODE))->GetCheck() == BST_CHECKED);
	}
#endif

	{
		bool enable = false;
		WORD type = 0;

		//EtronDI_GetSensorRegister(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo, int nId, unsigned short address, unsigned short *pValue, int flag, int nSensorMode);
		//EtronDI_GetFWRegister(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo, unsigned short address, unsigned short *pValue, int flag);
		//EtronDI_GetHWRegister(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo, unsigned short address, unsigned short *pValue, int flag);
		//EtronDI_GetFwVersion(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo, char *pszFwVersion, int nBufferSize, int *pActualLength);
		//EtronDI_GetPidVid(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo, unsigned short *pPidBuf, unsigned short *pVidBuf);
		//EtronDI_GetSerialNumber(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo, BYTE *pSerialNum, int nBufferSize, int *pACtualSNLenByByte);
		//EtronDI_GetLogData(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo, BYTE *buffer, int BufferLength, int *pActualLength, int index);
		//EtronDI_GetUserData(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo, BYTE *buffer, int BufferLength, USERDATA_SECTION_INDEX usi);
		//EtronDI_GetFileData(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo, int nID, int DataSize, BYTE *lpData);
		//EtronDI_GetImage(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo,
		//	BYTE *pBuf, unsigned long int *pImageSize, int *pSerial = NULL)
		//EtronDI_Get2Image(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo,
		//	BYTE *pImageLBuf, BYTE *pDepthBuf,
		//	unsigned long int *pColorImageSize, unsigned long int *pDepthImageSize,
		//	int *pSerial = NULL, int *pSerial2 = NULL);
		//EtronDI_Get2ImageEx(void* pHandleEtronDI, PDEVSELINFO pDevSelInfo,
		//	BYTE* imgBuf, unsigned long int* imgSize, int* imgSerial,
		//	int depthCount, BYTE** depthBuf, unsigned long int* depthSize, int* depthSerial);
		//EtronDI_GetDeviceResolutionList(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo,
		//	int nMaxCount0, ETRONDI_STREAM_INFO *pStreamInfo0,
		//	int nMaxCount1, ETRONDI_STREAM_INFO *pStreamInfo1);
		//DSG(1, "EtronDI_Is360Device(m_hEtronDI, &m_DevSelInfo)=%d", EtronDI_Is360Device(m_hEtronDI, &m_DevSelInfo));
		//EtronDI_GetResolution_ColorAndDepth(m_hEtronDI, &m_DevSelInfo, int *pcxColor, int *pcyColor, int *pcxDepth, int *pcyDepth);
		//EtronDI_GetSerialNumberFromLog(void *pHandleEtronDI, PDEVSELINFO pDevSelInfo, char *pSerialNum, int nBufferSize, int *pActualLength);

		EtronDI_GetCurrentIRValue(m_hEtronDI, &m_DevSelInfo, &type);
		DSG(1, "EtronDI_GetCurrentIRValue type=%d", type);
		WORD first;
		WORD second;
		EtronDI_GetIRMinValue(m_hEtronDI, &m_DevSelInfo, &first);
		EtronDI_GetIRMaxValue(m_hEtronDI, &m_DevSelInfo, &second);
		DSG(1, "EtronDI_GetIRMinValue EtronDI_GetIRMaxValue first=%d second=%d", first, second);

		EtronDI_GetIRMode(m_hEtronDI, &m_DevSelInfo, &type);
		DSG(1, "EtronDI_GetIRMode type=%d", type);

		int sensorType = 0;
		float expTime = 0.0;
		EtronDI_GetExposureTime(m_hEtronDI, &m_DevSelInfo, sensorType, &expTime);
		DSG(1, "EtronDI_GetExposureTime expTime=%f", expTime);
		float fGlobalGain = 0.0;
		EtronDI_GetGlobalGain(m_hEtronDI, &m_DevSelInfo, sensorType, &fGlobalGain);
		DSG(1, "EtronDI_GetGlobalGain fGlobalGain=%f", fGlobalGain);
		float fGainR = 0.0;
		float fGainG = 0.0;
		float fGainB = 0.0;
		EtronDI_GetColorGain(m_hEtronDI, &m_DevSelInfo, sensorType, &fGainR, &fGainG, &fGainB);
		DSG(1, "EtronDI_GetColorGain fGainR=%f", fGainR);
		DSG(1, "EtronDI_GetColorGain fGainG=%f", fGainG);
		DSG(1, "EtronDI_GetColorGain fGainB=%f", fGainB);
		unsigned short mode;
		EtronDI_GetAutoExposureMode(m_hEtronDI, &m_DevSelInfo, &mode);
		DSG(1, "EtronDI_GetAutoExposureMode mode=%d", mode);
		unsigned short GyroLen;
		EtronDI_GetFlexibleGyroLength(m_hEtronDI, &m_DevSelInfo, &GyroLen);
		DSG(1, "EtronDI_GetFlexibleGyroLength GyroLen=%d", GyroLen);
		EtronDI_GetDepthDataType(m_hEtronDI, &m_DevSelInfo, &type);
		DSG(1, "EtronDI_GetDepthDataType type=%d", type);

		EtronDI_GetHWPostProcess(m_hEtronDI, &m_DevSelInfo, &enable);
		DSG(1, "EtronDI_GetHWPostProcess enable=%d", enable);
		DSG(1, "EtronDI_HasMultiDepth0ColorPlusDepth enable=%d", EtronDI_HasMultiDepth0ColorPlusDepth(m_hEtronDI, &m_DevSelInfo, 0));
		DSG(1, "EtronDI_HasMultiDepth2 enable=%d", EtronDI_HasMultiDepth2(m_hEtronDI, &m_DevSelInfo));
		DSG(1, "EtronDI_IsDeocclusionDevice enable=%d", EtronDI_IsDeocclusionDevice(m_hEtronDI, &m_DevSelInfo));
		DSG(1, "EtronDI_IsInterleaveDevice enable=%d", EtronDI_IsInterleaveDevice(m_hEtronDI, &m_DevSelInfo));

		ret = true;
	}

	return ret;
}

void DepthCamera::DeinitHardware()
{
	FreeDepthParams();
}

bool DepthCamera::InitHardwareInternal()
{
	if (m_previewParams.m_colorOption >= 0) //color
	{
		cv::Rect colorRealRes = CurColorImgRes();
		LONG width = colorRealRes.width;
		LONG height = colorRealRes.height;
#ifdef ESPDI_EG
		if (false)
#else
		if (EtronDI_HasMultiDepth0ColorPlusDepth(m_hEtronDI, &m_DevSelInfo, m_previewParams.m_colorOption))
#endif
		{
			width = width / 2;
		}
		else if (m_previewParams.m_360ModeEnabled)
		{
			EnableDenoise(true);

			InitEysov(width, height);
		}
		colorRealRes.width = width;
		colorRealRes.height = height;
		DSG(1, "DepthCamera::InitHardwareInternal width=%d height=%d", width, height);

		m_previewParams.m_colorPreview.m_imgRes = colorRealRes;
	}
	KneronFace3DGrabber::GlobalCacheData *pGlobalCacheData;
#ifdef OWNER_IS_KNERON_FACE3DGRABBER
	pGlobalCacheData = KneronFace3DGrabber::GlobalCacheData::instance();
#else
	pGlobalCacheData = m_pGlobalCacheData;
#endif
	EtronDIImageType::Value depthImageType = EtronDIImageType::DepthDataTypeToDepthImageType(m_previewParams.m_depthType);
	DSG(1, "DepthCamera::InitHardwareInternal depthImageType=%d", depthImageType);//100, 102
	for (int i = 0; i < ETronDI_MAX_DEPTH_STREAM_COUNT; ++i)//depth
	{
		DSG(1, "pGlobalCacheData->devices_depth_resolution_idx=%d", pGlobalCacheData->devices_depth_resolution_idx);
		DSG(1, "DepthCamera::InitHardwareInternal m_previewParams.m_depthOption=%d", m_previewParams.m_depthOption); // 0
		DSG(1, "DepthCamera::InitHardwareInternal m_previewParams.m_depthSwitch=%d", m_previewParams.m_depthSwitch); // 2
		if (m_previewParams.m_depthOption >= 0 && (m_previewParams.m_depthSwitch & (1 << i)) != 0)
		{
			DSG(1, "i=%d", i);
			//if (i == pGlobalCacheData->devices_depth_resolution_idx)
			//{
				DSG(1, "DepthCamera::InitHardwareInternal over");
				cv::Rect res = CurDepthImgRes(i);
				int zdTableIndex = GetDepthStreamIndex(res);
				DSG(1, "DepthCamera::InitHardwareInternal res.x=%d", res.width);//320
				DSG(1, "DepthCamera::InitHardwareInternal res.y=%d", res.height);//480
				DSG(1, "DepthCamera::InitHardwareInternal zdTableIndex=%d", zdTableIndex);//0
				//width: 1280, seem to have some problem. 
				//if (res.width() > 640)
				//	continue;

				if (i != 0 && depthImageType == EtronDIImageType::DEPTH_8BITS)
				{
					res.width = res.width * 2;
				}
				DSG(1, "DepthCamera::InitHardwareInternal i=%d res.x=%d", i, res.width);//1, 640
				m_previewParams.m_depthPreview[i].m_imgRes = res;//1
				DSG(1, "DepthCamera::InitHardwareInternal m_previewParams.m_depthPreview[%d].m_imgRes=%d %d", i, m_previewParams.m_depthPreview[i].m_imgRes.width, m_previewParams.m_depthPreview[i].m_imgRes.height);//1 640

				ResetDepthParams();

				m_nDepthResWidth = res.width;
				m_nDepthResHeight = res.height;
				m_depthId = i; //i
				m_zdTableIndex = zdTableIndex;
				m_camFocus = m_previewParams.m_camFocus; //m_previewParams.m_camFocus
				m_baselineDist = m_previewParams.m_baselineDist[i]; //m_previewParams.m_baselineDist[i]
				m_DepthMapType = m_previewParams.m_depthMapType; //m_previewParams.m_depthMapType
				m_depthImageType = depthImageType;
				m_nDepthColorMapMode = m_previewParams.m_nDepthColorMapMode;//m_previewParams.m_nDepthColorMapMode
				//DmColorMode(m_ColorPalette, m_nDepthColorMapMode);
				DSG(1, "CDepthDlg::SetDepthImageParams m_DevSelInfo.index=%d", m_DevSelInfo.index);//0
				DSG(1, "CDepthDlg::SetDepthImageParams m_DevType=%d", m_DevType);//1
				DSG(1, "CDepthDlg::SetDepthImageParams m_DepthMapType=%d", m_DepthMapType);//0
				DSG(1, "CDepthDlg::SetDepthImageParams m_depthImageType=%d", m_depthImageType);//100
				DSG(1, "CDepthDlg::SetDepthImageParams m_depthId=%d", m_depthId);//1
				DSG(1, "CDepthDlg::SetDepthImageParams m_zdTableIndex=%d", m_zdTableIndex);//0
				DSG(1, "CDepthDlg::SetDepthImageParams m_nDepthResWidth=%d", m_nDepthResWidth);//640
				DSG(1, "CDepthDlg::SetDepthImageParams m_nDepthResHeight=%d", m_nDepthResHeight);//480
				DSG(1, "CDepthDlg::SetDepthImageParams m_camFocus=%f", m_camFocus);//0000
				DSG(1, "CDepthDlg::SetDepthImageParams m_baselineDist=%f", m_baselineDist);//0000
				DSG(1, "CDepthDlg::SetDepthImageParams m_nDepthColorMapMode=%d", m_nDepthColorMapMode);//2

				//if (!multiBaselineDist.empty())
				//{
				//	std::vector<double> multiplier;
				//	for (auto dist : multiBaselineDist)
				//	{
				//		if (dist == m_baselineDist)
				//		{
				//			multiplier.push_back(1.0);
				//		}
				//		else
				//		{
				//			multiplier.push_back(dist / m_baselineDist);
				//		}
				//	}
				//	m_mblDistMultiplier = multiplier;
				//}
				//else
				//{
				//	m_mblDistMultiplier.clear(); //here
				//}

				AllocDepthParams();

				//break;
			//}
			break;
		}
	}

	//false
	if (m_previewParams.m_IsDepthFusionOn)//depth fusion
	{

	}
	return true;
}

void DepthCamera::ResetDepthParams() 
{
	m_pImageBuf = NULL;

	int i;

	for (i = 0; i<256; i++) {
		m_GrayPalette[i][0] = (BYTE)i;
		m_GrayPalette[i][1] = (BYTE)i;
		m_GrayPalette[i][2] = (BYTE)i;
		m_GrayPalette[i][3] = 0;
	}

	m_nDepthResWidth = 0;
	m_nDepthResHeight = 0;

	m_DepthMapType = TRANSFER_TO_COLORFULRGB;
	m_depthImageType = EtronDIImageType::DEPTH_8BITS;// EtronDIImageType::DEPTH_11BITS;

	m_depthId = -1;
	//m_depthSerialNumber = -1;
	//m_zdTable = NULL;
	m_zdTableIndex = 0;
	m_camFocus = 0.0;
	m_baselineDist = 0.0;
}

bool DepthCamera::AllocDepthParams() 
{
	if (m_depthImageType == EtronDIImageType::DEPTH_8BITS)
	{
		m_depthData.resize(m_nDepthResWidth * m_nDepthResHeight, 0);
	}
	else
	{
		m_depthData.resize(m_nDepthResWidth * m_nDepthResHeight * 2, 0);
	}
	DSG(1, "DepthCamera::AllocDepthParams m_depthImageType=%d m_nDepthResWidth=%d m_nDepthResHeight=%d", 
		m_depthImageType, m_nDepthResWidth, m_nDepthResHeight);
	m_pImageBuf = (unsigned char*)realloc(m_pImageBuf, m_nDepthResWidth * m_nDepthResHeight * 3 * sizeof(unsigned char));
	
	DmColorMode(m_ColorPalette, m_nDepthColorMapMode);
	if (m_DevType == PUMA)
	{
		DmColorMode11(m_ColorPaletteD11, m_nDepthColorMapMode);
		SetBaseGrayPaletteD11(m_GrayPaletteD11);
		//jammy SetBaseColorPaletteZ14(m_ColorPaletteZ14);//xx
		DmColorMode14(m_ColorPaletteZ14, m_nDepthColorMapMode);
		SetBaseGrayPaletteZ14(m_GrayPaletteZ14);
	}

	return true;

}

void DepthCamera::FreeDepthParams()
{
	if (m_pImageBuf != NULL) {
		free(m_pImageBuf);
		m_pImageBuf = NULL;
	}
}

bool DepthCamera::GetEtronDIDevice(std::vector<DEVINFORMATIONEX>& device)
{
	if (m_hEtronDI != NULL)//etronDI.hEtronDI != NULL)
	{
		int deviceCount = EtronDI_GetDeviceNumber(m_hEtronDI);// etronDI.hEtronDI);
		if (deviceCount > 0)
		{
			device.clear();
			DSG(1, "DepthCamera::GetEtronDIDevice deviceCount=%d", deviceCount);
			for (int i = 0; i < deviceCount; ++i)
			{
				DEVSELINFO devSelInfo;
				devSelInfo.index = i;

				m_DevSelInfo.index = i;
				DSG(1, "DepthCamera::GetEtronDIDevice m_DevSelInfo.index=%x", m_DevSelInfo.index);

				DEVINFORMATIONEX deviceInfo;
				//EtronDI_GetDeviceInfoEx(etronDI.hEtronDI, &devSelInfo, &deviceInfo);
				EtronDI_GetDeviceInfoEx(m_hEtronDI, &devSelInfo, &deviceInfo);
				m_DevType = deviceInfo.nDevType;
				DSG(1, "DepthCamera::GetEtronDIDevice m_DevType=%x", m_DevType);
				DSG(1, "DepthCamera::GetEtronDIDevice deviceInfo.nChipID=%x", deviceInfo.nChipID);
				DSG(1, "DepthCamera::GetEtronDIDevice deviceInfo.nDevType=%x", deviceInfo.nDevType);
				DSG(1, "DepthCamera::GetEtronDIDevice deviceInfo.nChipID=%s", deviceInfo.strDevName);
				DSG(1, "DepthCamera::GetEtronDIDevice deviceInfo.wPID=%x", deviceInfo.wPID);
				DSG(1, "DepthCamera::GetEtronDIDevice deviceInfo.wVID=%x", deviceInfo.wVID);
				device.push_back(deviceInfo);

				break;
			}

			return true;
		}
	}

	return false;
}

int  DepthCamera::GetDepthStreamIndex(cv::Rect depthRes) const
{
	for (int i = 0; i < m_depthStreamOptionCount; ++i)
	{
		const ETRONDI_STREAM_INFO& streamDepthInfo = m_pStreamDepthInfo[i];
		if (streamDepthInfo.nWidth == depthRes.width && streamDepthInfo.nHeight == depthRes.height)
		{
			return i;
		}
	}

	return -1;
}

void DepthCamera::DepthTypeToUIOption(WORD type, bool& rectify, int& depthComboIndex) const
{
	rectify = (type >= 1 && type <= 5);
#ifdef FOR_CAINI
	depthComboIndex = 0;
	return;
#endif

	switch (type)
	{
	case ETronDI_DEPTH_DATA_8_BITS:
	case ETronDI_DEPTH_DATA_8_BITS_RAW:
		depthComboIndex = 0;
		break;
	case ETronDI_DEPTH_DATA_8_BITS_x80:
	case ETronDI_DEPTH_DATA_8_BITS_x80_RAW:
		depthComboIndex = 1;
		break;
	case ETronDI_DEPTH_DATA_11_BITS:
	case ETronDI_DEPTH_DATA_11_BITS_RAW:
	case ETronDI_DEPTH_DATA_11_BITS_COMBINED_RECTIFY:
		depthComboIndex = 2;
		break;
	case ETronDI_DEPTH_DATA_14_BITS:
	case ETronDI_DEPTH_DATA_14_BITS_RAW:
		depthComboIndex = 3;
		break;
	default:
		depthComboIndex = -1;
		break;
	}
}

WORD DepthCamera::UIOptionToDepthType(bool rectify, int depthComboIndex)
{
#ifdef FOR_CAILI
	return ETronDI_DEPTH_DATA_11_BITS;
#endif
	if (rectify)
	{
		switch (depthComboIndex)
		{
		case 0: return ETronDI_DEPTH_DATA_8_BITS;
		case 1: return ETronDI_DEPTH_DATA_8_BITS_x80;
#ifdef ESPDI_EG
		case 2: return ETronDI_DEPTH_DATA_11_BITS;
#else
		case 2: return (EtronDI_HasMultiDepth2(m_hEtronDI, &m_DevSelInfo) ? ETronDI_DEPTH_DATA_11_BITS_COMBINED_RECTIFY : ETronDI_DEPTH_DATA_11_BITS);
#endif
		case 3: return ETronDI_DEPTH_DATA_14_BITS;
		default: return ETronDI_DEPTH_DATA_OFF_RECTIFY;
		}
	}
	else
	{
		switch (depthComboIndex)
		{
		case 0: return ETronDI_DEPTH_DATA_8_BITS_RAW;
		case 1: return ETronDI_DEPTH_DATA_8_BITS_x80_RAW;
#ifdef ESPDI_EG
		case 2: return ETronDI_DEPTH_DATA_11_BITS_RAW;
#else
		case 2: return (EtronDI_HasMultiDepth2(m_hEtronDI, &m_DevSelInfo) ? ETronDI_DEPTH_DATA_11_BITS_COMBINED_RECTIFY : ETronDI_DEPTH_DATA_11_BITS_RAW);
#endif
		case 3: return ETronDI_DEPTH_DATA_14_BITS_RAW;
		default: return ETronDI_DEPTH_DATA_OFF_RAW;
		}
	}
}

void DepthCamera::UpdatePreviewParams()
{
	KneronFace3DGrabber::GlobalCacheData *pGlobalCacheData;
#ifdef OWNER_IS_KNERON_FACE3DGRABBER
	pGlobalCacheData = KneronFace3DGrabber::GlobalCacheData::instance();
#else
	pGlobalCacheData = m_pGlobalCacheData;
#endif

	CAutoLock lock(&m_previewParams.m_mutex);

	m_previewParams.Reset();

	if (1)//((CButton*)GetDlgItem(IDC_CHECK_COLOR_STREAM))->GetCheck() == BST_CHECKED)
	{
		m_previewParams.m_colorOption = pGlobalCacheData->devices_color_resolution_idx;// 0;// ((CComboBox*)GetDlgItem(IDC_COMBO_COLOR_STREAM))->GetCurSel();
	}
	else
	{
		m_previewParams.m_colorOption = -1;
	}
	DSG(1, "m_previewParams.m_colorOption=%d\r\n", m_previewParams.m_colorOption);//0
	m_previewParams.m_IsDepthFusionOn = 0;// (((CButton*)GetDlgItem(IDC_CHECK_DEPTH_FUSION))->GetCheck() == BST_CHECKED);
	DSG(1, "m_previewParams.m_IsDepthFusionOn=%d\r\n", m_previewParams.m_IsDepthFusionOn);//0
	if (0)//((CButton*)GetDlgItem(IDC_CHECK_DEPTH0))->GetCheck() == BST_CHECKED)
	{
		m_previewParams.m_depthSwitch |= EtronDIDepthSwitch::Depth0;
	}
	DSG(1, "m_previewParams.m_depthSwitch=%d\r\n", m_previewParams.m_depthSwitch);//0
	if (1)//((CButton*)GetDlgItem(IDC_CHECK_DEPTH1))->GetCheck() == BST_CHECKED)
	{
		m_previewParams.m_depthSwitch |= EtronDIDepthSwitch::Depth1;
	}
	DSG(1, "m_previewParams.m_depthSwitch=%d\r\n", m_previewParams.m_depthSwitch);//2
	if (0)//((CButton*)GetDlgItem(IDC_CHECK_DEPTH2))->GetCheck() == BST_CHECKED)
	{
		m_previewParams.m_depthSwitch |= EtronDIDepthSwitch::Depth2;
	}
	DSG(1, "m_previewParams.m_depthSwitch=%d\r\n", m_previewParams.m_depthSwitch);//2
	if (m_previewParams.m_depthSwitch != 0)
	{
		
		DSG(1, "pGlobalCacheData->devices_depth_resolution_idx=%d\r\n", pGlobalCacheData->devices_depth_resolution_idx);																				  //CComboBox* pDepthBitComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_DEPTH_BIT_SEL_CTRL);
		m_previewParams.m_depthOption = pGlobalCacheData->devices_depth_resolution_idx;
	}
	else
	{
		m_previewParams.m_depthOption = -1;
	}
	DSG(1, "m_previewParams.m_depthOption=%d\r\n", m_previewParams.m_depthOption);//0
	m_previewParams.m_360ModeEnabled = 0;// ((CButton*)GetDlgItem(IDC_CHECK_360MODE))->GetCheck() == BST_CHECKED;
	DSG(1, "m_previewParams.m_360ModeEnabled=%d\r\n", m_previewParams.m_360ModeEnabled);//0
	DSG(1, "m_previewParams.m_ctrlMode=%d\r\n", m_previewParams.m_ctrlMode);//0
	if (m_previewParams.m_360ModeEnabled)
	{
		BIT_SET(m_previewParams.m_ctrlMode, 2);// 0x4: enable dewarp stitch
		BIT_SET(m_previewParams.m_ctrlMode, 3);// 0x8: enable opencl
	}
	else
	{
		BIT_CLEAR(m_previewParams.m_ctrlMode, 2);
		BIT_CLEAR(m_previewParams.m_ctrlMode, 3);
	}
	DSG(1, "m_previewParams.m_ctrlMode=%d\r\n", m_previewParams.m_ctrlMode);//0
																			//UpdatePostProcessParam();
	m_previewParams.m_postProcess = 0;// ((CButton*)GetDlgItem(IDC_CHECK_PP))->GetCheck() == BST_CHECKED;
	if (m_previewParams.m_postProcess)
	{
		BIT_SET(m_previewParams.m_ctrlMode, 1);// 0x2: enable post process
	}
	else
	{
		BIT_CLEAR(m_previewParams.m_ctrlMode, 1);
	}
	DSG(1, "UpdatePostProcessParam m_previewParams.m_postProcess=%d\r\n", m_previewParams.m_postProcess);//0
	DSG(1, "UpdatePostProcessParam m_previewParams.m_ctrlMode=%d\r\n", m_previewParams.m_ctrlMode);//0


	m_previewParams.m_rectifyData = gRectify;// (((CButton*)GetDlgItem(IDC_RADIO_RECTIFY_DATA))->GetCheck() == BST_CHECKED);
	DSG(1, "m_previewParams.m_rectifyData=%d\r\n", m_previewParams.m_rectifyData);
	m_previewParams.m_camFocus = 0;// getDoubleValue(IDC_EDIT_CAM_FOCUS);
	m_previewParams.m_baselineDist[0] = 0;//getDoubleValue(IDC_EDIT_BLDIST0);
	m_previewParams.m_baselineDist[1] = 0;//getDoubleValue(IDC_EDIT_BLDIST1);
	m_previewParams.m_baselineDist[2] = 0;//getDoubleValue(IDC_EDIT_BLDIST2);
	DSG(1, "m_previewParams.m_camFocus=%f\r\n", m_previewParams.m_camFocus);//0
	DSG(1, "m_previewParams.m_baselineDist[0]=%f\r\n", m_previewParams.m_baselineDist[0]);//0
	DSG(1, "m_previewParams.m_baselineDist[1]=%f\r\n", m_previewParams.m_baselineDist[1]);//0
	DSG(1, "m_previewParams.m_baselineDist[2]=%f\r\n", m_previewParams.m_baselineDist[2]);//0
	
	DSG(1, "pGlobalCacheData->devices_depth_mapbits_idx=%d\r\n", pGlobalCacheData->devices_depth_mapbits_idx);																				  //CComboBox* pDepthBitComboBox = (CComboBox*)GetDlgItem(IDC_COMBO_DEPTH_BIT_SEL_CTRL);
	if (pGlobalCacheData->devices_depth_mapbits_idx >= 0)
	{
		//m_previewParams.m_depthType = UIOptionToDepthType(m_previewParams.m_rectifyData, 0);// pDepthBitComboBox->GetCurSel());
//#ifdef USE_ETRON_14BITS_DEVICE
//		m_previewParams.m_depthType = UIOptionToDepthType(m_previewParams.m_rectifyData, 2);// pDepthBitComboBox->GetCurSel());
//#else
//		m_previewParams.m_depthType = UIOptionToDepthType(m_previewParams.m_rectifyData, 0);// pDepthBitComboBox->GetCurSel());
//#endif
		m_previewParams.m_depthType = 
			UIOptionToDepthType(m_previewParams.m_rectifyData, pGlobalCacheData->devices_depth_mapbits_idx);
		
	}
	DSG(1, "m_previewParams.m_depthType=%d\r\n", m_previewParams.m_depthType);//6
	if (1)//((CButton*)GetDlgItem(IDC_CHECK_COLOR_STREAM))->GetCheck() == BST_CHECKED && ((CComboBox*)GetDlgItem(IDC_COMBO_COLOR_OUTPUT_CTRL))->GetCurSel() == 1)
	{
		m_previewParams.m_isImgRGB = TRUE;
	}
	else
	{
		m_previewParams.m_isImgRGB = FALSE;
	}
	DSG(1, "m_previewParams.m_isImgRGB=%d\r\n", m_previewParams.m_isImgRGB);// 0 or 1
	if (0)//((CComboBox*)GetDlgItem(IDC_COMBO_DEPTH_OUTPUT_CTRL))->GetCurSel() == 0)
	{
		m_previewParams.m_depthMapType = TRANSFER_TO_COLORFULRGB;
		//0:default, 1: Near, 2: Middle, 3: far
		m_previewParams.m_nDepthColorMapMode = 0;// ((CComboBox*)GetDlgItem(IDC_COMBO_DEPTH_COLOR_MODE))->GetCurSel();
	}
	else
	{
		m_previewParams.m_depthMapType = TRANSFER_TO_GRAYRGB;
	}
	DSG(1, "m_previewParams.m_depthMapType=%d\r\n", m_previewParams.m_depthMapType);//0
	DSG(1, "m_previewParams.m_nDepthColorMapMode=%d\r\n", m_previewParams.m_nDepthColorMapMode);//0 or 2
}

bool DepthCamera::UpdateStreamInfo()
{
	memset(m_pStreamColorInfo, 0, sizeof(ETRONDI_STREAM_INFO) * ETronDI_MAX_STREAM_COUNT);
	memset(m_pStreamDepthInfo, 0, sizeof(ETRONDI_STREAM_INFO) * ETronDI_MAX_STREAM_COUNT);

	int ret = EtronDI_GetDeviceResolutionList(m_hEtronDI, &m_DevSelInfo, ETronDI_MAX_STREAM_COUNT, m_pStreamColorInfo, ETronDI_MAX_STREAM_COUNT, m_pStreamDepthInfo);
	m_colorStreamOptionCount = (int)((BYTE*)(&ret))[1];
	m_depthStreamOptionCount = (int)((BYTE*)(&ret))[0];
	DSG(1, "UpdateStreamInfo m_colorStreamOptionCount=%d m_depthStreamOptionCount =%d",
		m_colorStreamOptionCount, m_depthStreamOptionCount);

	return false;
}

void DepthCamera::UpdateIRConfig()
{
	EtronDI_GetIRMinValue(m_hEtronDI, &m_DevSelInfo, &m_irRange.first);
	EtronDI_GetIRMaxValue(m_hEtronDI, &m_DevSelInfo, &m_irRange.second);
}

void DepthCamera::UpdateUI()
{
	DSG(1, "DepthCamera::UpdateUI() m_colorStreamOptionCount=%d", m_colorStreamOptionCount);

	bool support360 = EtronDI_Is360Device(m_hEtronDI, &m_DevSelInfo);
	DSG(1, "DepthCamera::UpdateUI() support360=%d", support360);
	DSG(1, "DepthCamera::UpdateUI() m_depthStreamOptionCount=%d", m_depthStreamOptionCount);

	for (int i = 0; i < m_depthStreamOptionCount; ++i)
	{
		const ETRONDI_STREAM_INFO& streamDepthInfo = m_pStreamDepthInfo[i];
		DSG(1, "DepthCamera::UpdateUI() depth width=%d height=%d mjpeg=%d", streamDepthInfo.nWidth, streamDepthInfo.nHeight, streamDepthInfo.bFormatMJPG);
	}

	if (m_depthStreamOptionCount > 0)
		gDepthBitsComboIdx = 0;

#ifdef ESPDI_EG
	GetDlgItem(IDC_CHECK_DEPTH2)->EnableWindow(FALSE);
#else
	//depth2
	if (EtronDI_HasMultiDepth2(m_hEtronDI, &m_DevSelInfo))
	{
		DSG(1, "DepthCamera::UpdateUI() EtronDI_HasMultiDepth2 Yes");
	}
	else
	{
		DSG(1, "DepthCamera::UpdateUI() EtronDI_HasMultiDepth2 No");
	}
#endif

#ifndef ESPDI_EG
	//HW post process
	DSG(1, "DepthCamera::UpdateUI() IsSupportHwPostProc()=%d", (m_DevType == PUMA));
#endif

#ifndef ESPDI_EG
	if (EtronDI_HasMultiDepth2(m_hEtronDI, &m_DevSelInfo))
	{
		if (EtronDI_IsDeocclusionDevice(m_hEtronDI, &m_DevSelInfo))
		{
			DSG(1, "DepthCamera::UpdateUI() EtronDI_IsDeocclusionDevice YES");
		}
		else
		{
			DSG(1, "DepthCamera::UpdateUI() EtronDI_IsDeocclusionDevice No"); //here

																			  //SetDlgItemText(IDC_EDIT_CAM_FOCUS, "800");
																			  //SetDlgItemText(IDC_EDIT_BLDIST0, "30");
																			  //SetDlgItemText(IDC_EDIT_BLDIST1, "60");
																			  //SetDlgItemText(IDC_EDIT_BLDIST2, "150");
		}
	}
#endif

#ifdef ESPDI_EG
	GetDlgItem(IDC_COMBO_DEPTH_BIT_SEL_CTRL)->EnableWindow(FALSE);
#else

	switch (m_DevType)
	{
	case PUMA:
	{
		WORD wType = 0;
		if (EtronDI_GetDepthDataType(m_hEtronDI, &m_DevSelInfo, &wType) != ETronDI_OK)
		{
			DSG(1, "DepthCamera::UpdateUI() EtronDI_GetDepthDataType failed !!");
		}

		bool rectify = false;
		int depthComboIndex = -1;
		DepthTypeToUIOption(wType, rectify, depthComboIndex);
		DSG(1, "DepthCamera::UpdateUI() rectify=%d depthComboIndex=%d", rectify, depthComboIndex);
		if (depthComboIndex < 0)
		{
			depthComboIndex = 1;
			DSG(1, "DepthCamera::UpdateUI() after depthComboIndex=%d", depthComboIndex);
			if (EtronDI_SetDepthDataType(m_hEtronDI, &m_DevSelInfo, ETronDI_DEPTH_DATA_8_BITS_x80) != ETronDI_OK)
			{
				DSG(1, "DepthCamera::UpdateUI() EtronDI_SetDepthDataType failed !!");
			}
		}

		gDepthBitsComboIdx = depthComboIndex;

		DSG(1, "DepthCamera::UpdateUI() IsMultibaselineDevice()=%d", IsMultibaselineDevice());
		if (IsMultibaselineDevice())
		{
			depthComboIndex = 2;
		}

		gRectify = rectify;
		DSG(1, "DepthCamera::UpdateUI() gRectify=%d", gRectify);
	}
	break;

	case AXES1:
	default:
	{

	}
	break;
	}
#endif

	DSG(1, "DepthCamera::UpdateUI() m_irRange.first=%d", m_irRange.first);
	DSG(1, "DepthCamera::UpdateUI() m_irRange.second=%d", m_irRange.second);
	if ((m_irRange.second > m_irRange.first) && (0 == m_irRange.first))
		SetIRValue(1);
	else
		SetIRValue(m_irRange.first);

#ifndef ESPDI_EG
	if (EtronDI_IsInterleaveDevice(m_hEtronDI, &m_DevSelInfo))
	{
		DSG(1, "DepthCamera::UpdateUI() EtronDI_IsInterleaveDevice yes");
		SetIRValue(m_irRange.first + 1);
	}
	else
#endif
	{
		DSG(1, "DepthCamera::UpdateUI() EtronDI_IsInterleaveDevice no");
	}

	for (int i = 0; i < ETronDI_MAX_DEPTH_STREAM_COUNT; ++i)//depth
	{
		DSG(1, "DepthCamera::UpdateUI() m_previewParams.m_depthOption=%d", m_previewParams.m_depthOption); // -1
		DSG(1, "DepthCamera::UpdateUI() m_previewParams.m_depthSwitch=%d", m_previewParams.m_depthSwitch); // 0
		if (m_previewParams.m_depthOption >= 0 && (m_previewParams.m_depthSwitch & (1 << i)) != 0)
		{

		}
	}
}

cv::Rect DepthCamera::CurColorImgRes()
{
	CAutoLock lock(&m_previewParams.m_mutex);
	if (m_previewParams.m_colorOption >= 0)
	{
		const ETRONDI_STREAM_INFO& streamColorInfo = m_pStreamColorInfo[m_previewParams.m_colorOption];

		return cv::Rect(0, 0, streamColorInfo.nWidth, streamColorInfo.nHeight);
	}

	return cv::Rect(0, 0, 0, 0);
}

cv::Rect DepthCamera::CurDepthImgRes(int depthIndex)
{
	CAutoLock lock(&m_previewParams.m_mutex);

	DSG(1, "DepthCamera::CurDepthImgRes depthIndex=%d", depthIndex); //1
	DSG(1, "DepthCamera::CurDepthImgRes m_previewParams.m_depthOption=%d", m_previewParams.m_depthOption); // 0
	if (m_previewParams.m_depthOption >= 0)
	{
		if (depthIndex == 0)
		{
			cv::Rect res = CurColorImgRes();
			DSG(1, "DepthCamera::CurDepthImgRes 1 x=%d y=%d", res.width / 2, res.height);
			return cv::Rect(0, 0, res.width / 2, res.height);
		}
		else
		{
			const ETRONDI_STREAM_INFO& streamDepthInfo = m_pStreamDepthInfo[m_previewParams.m_depthOption];
			DSG(1, "DepthCamera::CurDepthImgRes 2 streamDepthInfo.nWidth=%d  streamDepthInfo.nHeight=%d", streamDepthInfo.nWidth, streamDepthInfo.nHeight); //320 480
			return cv::Rect(0, 0, streamDepthInfo.nWidth, streamDepthInfo.nHeight);
		}
	}

	return cv::Rect(0, 0, 0, 0);
}

int DepthCamera::EnableDenoise(bool enable)
{
	unsigned short value = 0;
#ifndef IGN_SETTING
	int ret = EtronDI_GetHWRegister(m_hEtronDI, &m_DevSelInfo, 0xf101, &value, FG_Address_2Byte | FG_Value_1Byte);
	if (ret != ETronDI_OK)
	{
		return ret;
	}

	if (enable)
	{
		BIT_CLEAR(value, 2);
	}
	else
	{
		BIT_SET(value, 2);
	}

	return EtronDI_SetHWRegister(m_hEtronDI, &m_DevSelInfo, 0xf101, value, FG_Address_2Byte | FG_Value_1Byte);
#else
	return 0;
#endif
}

int DepthCamera::InitEysov(LONG& outWidth, LONG& outHeight)
{
#ifdef ESPDI_EG
	PARALUT paraLut;
	int ret = EtronDI_GetLutParam(m_hEtronDI, &m_DevSelInfo, CurColorOption(), &paraLut);
	if (ret == ETronDI_OK)
	{
		//enable dewarp stitch
		ret = EtronDI_GenerateLut(m_hEtronDI, &m_DevSelInfo, &paraLut);
		if (ret == ETronDI_OK)
		{
			outWidth = (LONG)paraLut.out_img_cols;
			outHeight = (LONG)paraLut.out_img_rows;
		}
	}

	return ret;
#else
	return ETronDI_DEVICE_NOT_SUPPORT;
#endif
}

void DepthCamera::GetPreviewImage(cv::Mat &matImage)
{
	m_imgPreviewList.Popup(matImage);
}

void DepthCamera::GetDepthImage(cv::Mat &matImage)
{
	m_imgDepthList.Popup(matImage);
}

bool DepthCamera::IsMultibaselineDevice()
{
#ifdef ESPDI_EG
	return false;
#else
	// assume the first color stream (index is 0) is the image of (color + depth)
	return EtronDI_HasMultiDepth0ColorPlusDepth(m_hEtronDI, &m_DevSelInfo, 0);
#endif
}

void DepthCamera::ApplyImagePreview(unsigned char *pDataBuf, int dataSize, BOOL bIsOutputRGB, BOOL bIsMJPEG, int nColorSerialNum)
{
	if (pDataBuf)
	{
		int width = m_previewParams.m_colorPreview.m_imgRes.width;
		int height = m_previewParams.m_colorPreview.m_imgRes.height;
        //DSG(1, "ApplyImagePreview width=%d", width);
        //DSG(1, "ApplyImagePreview height=%d", height);

		//#ifdef QT_VERSION
		//	QMutexLocker locker(&m_mutexPreview);
		//#else
		//	std::lock_guard<std::mutex> lock(m_mutexPreview);
		//#endif

		cv::Mat matFrame = cv::Mat(height, width, CV_8UC3, pDataBuf);
		//cv::Mat matDst;
		//#if 0
		//	//cvtColor(matFrame, matDst, cv::COLOR_BGR2RGB);
		//	//cvtColor(matFrame, matDst, cv::COLOR_RGB2BGR);
		//#else
		//	matDst = matFrame;
		//#endif

			cv::Mat matPreview;
		#if defined(GIVE_ME_MIRROR) && !defined(USE_HW_MIRROR)
			cv::flip(matFrame, matPreview, -1);
		#else
			matPreview = matFrame;
		#endif

		#ifdef OWNER_IS_KNERON_FACE3DGRABBER
		{
			QMutexLocker locker(&m_mutexPreview);
			emit matColorReady(matPreview);
		}
		#else
		{
			//DSG(1, "m_imgPreviewList.Insert");
			m_imgPreviewList.Insert(matPreview);
		}
		#endif
	}
}

void DepthCamera::ApplyImageCapture(unsigned char *pDataBuf, int nDepthSerialNum)
{
	if (pDataBuf)
	{
	//#ifdef QT_VERSION
	//	QMutexLocker locker(&m_mutexCapture);
	//#else
	//	std::lock_guard<std::mutex> lock(m_mutexCapture);
	//#endif

		cv::Mat matFrame;

		if (m_depthImageType == EtronDIImageType::DEPTH_8BITS)
		{
			matFrame = cv::Mat(m_nDepthResHeight, m_nDepthResWidth, CV_8U, pDataBuf);
			//DSG(1, "ApplyImageCapture xxxx 1");
		}
		else
		{
			matFrame = cv::Mat(m_nDepthResHeight, m_nDepthResWidth, CV_16U, pDataBuf);
			//DSG(1, "ApplyImageCapture xxxx 2");
		}
		//DSG(1, "ApplyImageCapture AA matFrame.cols=%d matFrame.rows=%d matFrame.elemSize()=%d", 
		//	matFrame.cols, matFrame.rows, matFrame.elemSize());

		cv::Mat matCapture;
	#if defined(GIVE_ME_MIRROR) && !defined(USE_HW_MIRROR)
		cv::flip(matFrame, matCapture, 1);
	#else
		matCapture = matFrame;
	#endif	

	#ifdef OWNER_IS_KNERON_FACE3DGRABBER
		{
			QMutexLocker locker(&m_mutexCapture);
			emit matDepthReady(matCapture);
		}
	#else
		{
			//DSG(1, "m_imgDepthList.Insert");
			m_imgDepthList.Insert(matCapture);
		}
	#endif
	}
}

void DepthCamera::ImgCallback(EtronDIImageType::Value imgType, int imgId, unsigned char* imgBuf, int imgSize,
	int width, int height, int serialNumber, void* pParam)
{
	if (!pParam) return;
	static int colorloopCnt = 0;
	static int depthloopCnt = 0;
	DepthCamera* pThis = (DepthCamera*)pParam;

	if (e_component_state_started != pThis->m_state)
		return;

	{
	    CAutoLock lock(&pThis->m_previewParams.m_mutex);

		if (EtronDIImageType::IsImageColor(imgType))
		{
			if (colorloopCnt == 30)
			{
				//qDebug("DepthCamera m_depthElapsedTimer=%lld imgId=%d", pThis->m_depthElapsedTimer.elapsed(), imgId);
				DSG(1, "ImgCallback imgId=%d imgSize=%d width=%d height=%d",
					imgId, imgSize, width, height);
				colorloopCnt = 0;
			}
			else
				++colorloopCnt;

			pThis->ApplyImagePreview(
				imgBuf, imgSize, imgType == EtronDIImageType::COLOR_RGB24, imgType == EtronDIImageType::COLOR_MJPG, serialNumber);
		}
		else if (EtronDIImageType::IsImageDepth(imgType))
		{
			if (depthloopCnt == 30)
			{
				//qDebug("DepthCamera m_depthElapsedTimer=%lld imgId=%d", pThis->m_depthElapsedTimer.elapsed(), imgId);
				DSG(1, "ImgCallback imgId=%d pThis->m_depthId=%d imgSize=%d width=%d height=%d",
					imgId, pThis->m_depthId, imgSize, width, height);
				depthloopCnt = 0;
			}
			else
				++depthloopCnt;

			if (imgId == pThis->m_depthId)
			{
				pThis->ApplyImageCapture(imgBuf, serialNumber);
			}
			else if (imgId == -1)// depth fusion
			{
			}
			else
			{
			}
		}
		else
		{
			DSG(1, "image callback failed. unknown image type");
		}
	}
}

void DepthCamera::start(int cam)
{
	DSG(1, "DepthCamera::start");

	if (Start())
	{
	#ifdef QT_ARCH
		emit started();
	#endif
	}
}

void DepthCamera::stop()
{
	Stop();
#ifdef QT_ARCH
	emit stopped();
#endif
}
#ifdef QT_VERSION
void DepthCamera::timerEvent(QTimerEvent * ev) {
	static int number_of_frames = 0;
	if (ev->timerId() != m_timerId) return;
	if (e_component_state_started != m_state) return;
}
#endif