#include "depthcamera.h"
#include "recordmanager_etron.h"
#include "etronapi.h"

void RecordingManager_ETron_SaveToBmp(void *pUserData, const cv::Mat &matSrc, const char *filename)
{
	if (!pUserData) return;
	if (!filename) return;

	imwrite(filename, matSrc);
}

void RecordingManager_ETron_ConvertToBmp24Bits(void *pUserData, const cv::Mat &matSrc, const char *filename)
{
	if (!pUserData) return;
	if (!filename) return;

	DepthCamera *p = reinterpret_cast<DepthCamera*>(pUserData);
	uchar *pSrcData = matSrc.data;
	DEPTHMAP_TYPE depthMapType = p->GetDepthMapType();
	EtronDIImageType::Value depthImageType = p->GetDepthImageType();

	int k_elements_palette = 0;
	BITMAPINFO *pBmpInfo = nullptr;
	if (depthImageType == EtronDIImageType::DEPTH_8BITS || depthImageType == EtronDIImageType::DEPTH_8BITS_0x80)
	{
		k_elements_palette = 256;
		pBmpInfo = (BITMAPINFO*)malloc(sizeof(BITMAPINFOHEADER) + k_elements_palette * sizeof(RGBQUAD));
	}
	else if (depthImageType == EtronDIImageType::DEPTH_11BITS)
	{
		k_elements_palette = 2048;
		pBmpInfo = (BITMAPINFO*)malloc(sizeof(BITMAPINFOHEADER));
	}
	else
	{
		k_elements_palette = 16384;
		pBmpInfo = (BITMAPINFO*)malloc(sizeof(BITMAPINFOHEADER));
	}

	if (!pBmpInfo) return;

	uchar *pDstData = p->GetImageBuffer();// m_pImageBuf;
										  //qDebug("RecordingManager::ConvertToBmp24Bits pSrcData=%p pDstData=%p", pSrcData, pDstData);
	int width = matSrc.cols;
	int height = matSrc.rows;

	unsigned int absHeight = abs(height);
	//qDebug("filename=%p", filename);
	//qDebug("R22=%s", filename.toLatin1().constData());
	size_t size;
	//size_t bytesofScanLine = (width % 4 == 0) ? width : ((width + 3) / 4 * 4);
	size_t actualSize;

	memset(&pBmpInfo->bmiHeader, 0, sizeof(BITMAPINFOHEADER));
	pBmpInfo->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pBmpInfo->bmiHeader.biWidth = width;
	pBmpInfo->bmiHeader.biHeight = height;
	pBmpInfo->bmiHeader.biPlanes = 1;
	pBmpInfo->bmiHeader.biCompression = BI_RGB;
	//pBmpInfo->bmiHeader.biSizeImage = actualSize;
	pBmpInfo->bmiHeader.biXPelsPerMeter = 175 * 10000 / 254;
	pBmpInfo->bmiHeader.biYPelsPerMeter = 175 * 10000 / 254;
	//qDebug("filename=%p", filename);
	//qDebug("R222=%s", filename.toLatin1().constData());
	BITMAPFILEHEADER bitmapFileHeader;
	bitmapFileHeader.bfType = 'B' + ('M' << 8);
	bitmapFileHeader.bfReserved1 = bitmapFileHeader.bfReserved2 = 0;
	//qDebug("filename=%p", filename);
	//qDebug("R2222=%s", filename.toLatin1().constData());
	int nBitsOffset;
	LONG lImageSize;
	LONG lFileSize;
	//640, 480, 1
	DSG(1, "RecordingManager::ConvertToBmp24Bits width=%d height=%d depthMapType=%d depthImageType=%d", width, height, depthMapType, depthImageType);

	if (depthImageType == EtronDIImageType::DEPTH_8BITS || depthImageType == EtronDIImageType::DEPTH_8BITS_0x80)
	{
		pBmpInfo->bmiHeader.biBitCount = 8;
		pBmpInfo->bmiHeader.biClrUsed = k_elements_palette;
		pBmpInfo->bmiHeader.biClrImportant = k_elements_palette;
		//qDebug("filename=%p", filename);
		//qDebug("R22222=%s", filename.toLatin1().constData());

		if (depthMapType == TRANSFER_TO_COLORFULRGB)
			memcpy(pBmpInfo->bmiColors, p->m_ColorPalette, sizeof(RGBQUAD) * k_elements_palette);
		else
			memcpy(pBmpInfo->bmiColors, p->m_GrayPalette, sizeof(RGBQUAD) * k_elements_palette);
		//qDebug("filename=%p", filename);
		//qDebug("Raaaa=%s", filename.toLatin1().constData());
		//actualSize = bytesofScanLine * absHeight;
		actualSize = width * height;// *3;
		pBmpInfo->bmiHeader.biSizeImage = actualSize;

		nBitsOffset = sizeof(BITMAPFILEHEADER) + pBmpInfo->bmiHeader.biSize + sizeof(RGBQUAD) * k_elements_palette;
		lImageSize = pBmpInfo->bmiHeader.biSizeImage;
		lFileSize = nBitsOffset + lImageSize;

		bitmapFileHeader.bfOffBits = nBitsOffset;
		bitmapFileHeader.bfSize = lFileSize;
	}
	else if (depthImageType == EtronDIImageType::DEPTH_11BITS)
	{
		pBmpInfo->bmiHeader.biBitCount = 24;
		pBmpInfo->bmiHeader.biClrUsed = k_elements_palette;
		pBmpInfo->bmiHeader.biClrImportant = k_elements_palette;

		//if (depthMapType == TRANSFER_TO_COLORFULRGB)
		//	memcpy(pBmpInfo->bmiColors, p->m_ColorPaletteD11, sizeof(RGBQUAD) * k_elements_palette);
		//else
		//	memcpy(pBmpInfo->bmiColors, p->m_GrayPaletteD11, sizeof(RGBQUAD) * k_elements_palette);
		//actualSize = bytesofScanLine * absHeight * 3;
		actualSize = width * height * 3;
		pBmpInfo->bmiHeader.biSizeImage = actualSize;

		nBitsOffset = sizeof(BITMAPFILEHEADER) + pBmpInfo->bmiHeader.biSize;
		lImageSize = pBmpInfo->bmiHeader.biSizeImage;
		lFileSize = nBitsOffset + lImageSize;

		bitmapFileHeader.bfOffBits = nBitsOffset;
		bitmapFileHeader.bfSize = lFileSize;
	}
	else {
		pBmpInfo->bmiHeader.biBitCount = 24;
		pBmpInfo->bmiHeader.biClrUsed = k_elements_palette;
		pBmpInfo->bmiHeader.biClrImportant = k_elements_palette;

		//if (depthMapType == TRANSFER_TO_COLORFULRGB)
		//	memcpy(pBmpInfo->bmiColors, p->m_ColorPaletteZ14, sizeof(RGBQUAD) * k_elements_palette);
		//else
		//	memcpy(pBmpInfo->bmiColors, p->m_GrayPaletteZ14, sizeof(RGBQUAD) * k_elements_palette);

		//actualSize = bytesofScanLine * absHeight * 3;
		actualSize = width * height * 3;
		pBmpInfo->bmiHeader.biSizeImage = actualSize;

		nBitsOffset = sizeof(BITMAPFILEHEADER) + pBmpInfo->bmiHeader.biSize;
		lImageSize = pBmpInfo->bmiHeader.biSizeImage;
		lFileSize = nBitsOffset + lImageSize;


		bitmapFileHeader.bfOffBits = nBitsOffset;
		bitmapFileHeader.bfSize = lFileSize;
	}
	//qDebug("filename=%p", filename);
	//qDebug("R3=%s", filename.toLatin1().constData());
	int x, y;
	BYTE *p0, *p1, *p2;
	//qDebug("RecordingManager::ConvertToBmp24Bits depthImageType=%d", depthImageType);//100
	switch (depthImageType)
	{
	case EtronDIImageType::DEPTH_8BITS:
		//qDebug("RecordingManager::ConvertToBmp24Bits absHeight=%d", absHeight);//480
		//qDebug("RecordingManager::ConvertToBmp24Bits width=%d", width);//640
		p0 = pSrcData + (absHeight - 1) * width;
		//qDebug("RecordingManager::ConvertToBmp24Bits p0[%d]=%x", 0, p0[0]);
		p1 = pDstData;
		//qDebug("filename=%p", filename);
		//qDebug("R4=%s", filename.toLatin1().constData());
		for (y = 0; y < absHeight; y++) {
			//qDebug("y=%d", y);
			memcpy(p1, p0, width);
			p0 -= width;
			p1 += width;
		}

		break;
	case EtronDIImageType::DEPTH_8BITS_0x80:
		p0 = pSrcData + (absHeight - 1) * width * 2;
		p1 = pDstData;
		for (y = 0; y < absHeight; y++) {
			p2 = p0;
			for (x = 0; x < width * 2; x += 2) {
				*p1 = *p2;
				p2 += 2;
				p1++;
			}
			p0 -= (width * 2);
		}
		break;
	case EtronDIImageType::DEPTH_11BITS:
	{
		if (depthMapType == TRANSFER_TO_COLORFULRGB)
			UpdateD11DisplayImage_DIB24(p->m_ColorPaletteD11, pSrcData, pDstData, width, absHeight);
		else
			UpdateD11DisplayImage_DIB24(p->m_GrayPaletteD11, pSrcData, pDstData, width, absHeight);
	}
	break;
	case EtronDIImageType::DEPTH_14BITS:
	{
		if (depthMapType == TRANSFER_TO_COLORFULRGB)
			UpdateZ14DisplayImage_DIB24(p->m_ColorPaletteZ14, pSrcData, pDstData, width, absHeight);
		else
			UpdateZ14DisplayImage_DIB24(p->m_GrayPaletteZ14, pSrcData, pDstData, width, absHeight);
		break;
	}
	}
	//qDebug("filename=%p", filename);
	//qDebug("RC=%s", filename.toLatin1().constData());
	FILE *fp = fopen(filename, "wb");
	if (fp)
	{
		fwrite(&bitmapFileHeader, sizeof(bitmapFileHeader), 1, fp);
		//qDebug("RecordingManager::ConvertToBmp24Bits bitmapFileHeader ok");
		fwrite(&pBmpInfo->bmiHeader, sizeof(pBmpInfo->bmiHeader), 1, fp);
		//qDebug("RecordingManager::ConvertToBmp24Bits bmiHeader ok");
		if (depthImageType == EtronDIImageType::DEPTH_8BITS || depthImageType == EtronDIImageType::DEPTH_8BITS_0x80)
		{
			fwrite(&(pBmpInfo->bmiColors[0]), sizeof(RGBQUAD) * k_elements_palette, 1, fp);
			//qDebug("RecordingManager::ConvertToBmp24Bits bmiColors ok");
			for (size_t j = 0; j < absHeight; ++j)
			{
				//fwrite(pDstData + (j * width * 3), 1, width * 3, fp);
				fwrite(pDstData + (j * width), 1, width, fp);
			}
		}
		else if (depthImageType == EtronDIImageType::DEPTH_11BITS)
		{
			DSG(1, "RecordingManager::ConvertToBmp24Bits DEPTH_11BITS bmiColors ok");

			//if (depthMapType == TRANSFER_TO_COLORFULRGB)
			//	fwrite(&(pBmpInfo->bmiColors[0]), sizeof(RGBQUAD) * k_elements_palette, 1, fp);
			//else
			//	fwrite(&(pBmpInfo->bmiColors[0]), sizeof(RGBQUAD) * k_elements_palette, 1, fp);

			//qDebug("RecordingManager::ConvertToBmp24Bits bmiColors ok");
			for (size_t j = 0; j < absHeight; ++j)
			{
				fwrite(pDstData + (j * width * 3), 1, width * 3, fp);
			}
		}
		else if (depthImageType == EtronDIImageType::DEPTH_14BITS)
		{
			//if (depthMapType == TRANSFER_TO_COLORFULRGB)
			//	fwrite(&(pBmpInfo->bmiColors[0]), sizeof(RGBQUAD) * k_elements_palette, 1, fp);
			//else
			//	fwrite(&(pBmpInfo->bmiColors[0]), sizeof(RGBQUAD) * k_elements_palette, 1, fp);

			//qDebug("RecordingManager::ConvertToBmp24Bits bmiColors ok");
			for (size_t j = 0; j < absHeight; ++j)
			{
				fwrite(pDstData + (j * width * 3), 1, width * 3, fp);
			}
		}

		//qDebug("RecordingManager::ConvertToBmp24Bits pDstData ok");
		fclose(fp);
	}

	free((void*)pBmpInfo);
}

void RecordingManager_ETron_ConvertToRaw(void *pUserData, const cv::Mat &matSrc, const char *filename)
{
	if (!pUserData) return;
	if (!filename) return;

	FILE *pFile = fopen(filename, "wb");
	if (pFile)
	{
		size_t size = matSrc.total() * matSrc.elemSize();
		fwrite((void const*)matSrc.data, 1, size, pFile);

		fclose(pFile);
		pFile = nullptr;
	}
}

