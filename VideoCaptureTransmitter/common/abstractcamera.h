#ifndef ABSTRACTCAMERA_H
#define ABSTRACTCAMERA_H

#include <stdint.h>
#include <vector>
#ifdef QT_VERSION
#include <QObject>
#include <qjsonobject.h>
#endif
#include "kneron_face3dgrabber.h"

typedef enum E_CaptureDevice_Type_Tag {
	e_devicetype_hmx_3d_camera = 0,
	e_devicetype_etron_3d_camera = 1
} ECaptureDeviceType;

typedef struct S_Camera_Capabilities_Tag {
	int32_t width;
	int32_t height;

} SCameraCapabilities;


typedef struct S_Camera_Config_Tag {
	uint32_t device_idx;
	uint32_t interval_ms;
	int32_t  want_width;
	int32_t  want_height;
	int32_t ir_value;
} SCameraConfig;

class AbstractCamera
#ifdef QT_VERSION
: public QObject
#endif
{
public:
#ifdef QT_VERSION
	AbstractCamera(EDeviceType deviceType, QObject *parent = {}): QObject(parent),
		m_devicetype(deviceType) {
		memset(&m_config, 0, sizeof(m_config));
	}
#else
	AbstractCamera(EDeviceType deviceType):
		m_devicetype(deviceType) {
		memset(&m_config, 0, sizeof(m_config));
	}
#endif
    virtual ~AbstractCamera(){};

	virtual bool SetConfig(SCameraConfig *pConfig) = 0;
    virtual bool Open() = 0;
	virtual void Close() = 0;
	virtual void Run() = 0;
	void operator()() { Run(); }

	std::vector<SCameraCapabilities>& GetCameraCapabilities() {
		return m_capabilitiesList;
	}

	EDeviceType GetDeviceType() { return m_devicetype; }

protected:
	SCameraConfig m_config;
	std::vector<SCameraCapabilities> m_capabilitiesList;

public:
	EDeviceType m_devicetype;
};


#endif // CAMERA_H
