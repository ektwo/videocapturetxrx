#ifndef KNERONFACE3DGRABBER_H
#define KNERONFACE3DGRABBER_H

#define KNERONFACE3DGRABBER_H_VERSION "0.3.9" 

#ifdef OWNER_IS_KNERON_FACE3DGRABBER
#include <QObject>
#include <QSettings>
#include <QApplication>
#include <QScreen>
#include <QtCore/qmath.h>
#include <QDir>
#include "singleton.h"
#endif

#include "cpp.hint"

#include <opencv2/opencv.hpp>
//#define IGN_SETTING
//#define SUPPORT_COVER_MOUTH
//#define DEBUG_SAVE_IMAGE
#define GIVE_ME_MIRROR
//#define USE_HW_MIRROR
//#define PREVIEW_DEPTH
#define PREVIEW_CAPTURE_FROM_DEPTH
#define USE_ETRON_14BITS_DEVICE

#define PREVIEW_RES_INDEX 3//m_previewParams.m_colorOption

#ifdef CALLER_IS_KNERON_FACE3DGRABBER
typedef void(*FnConvert)(void *, const cv::Mat &, QString);


static const char *k_str_mainwindow_prefix = "mainwindow_prefix";
static const char *k_str_mainwindow_location = "mainwindow_location";
static const char *k_str_general_settings_devices_dualscreen = "general_settings_devices_dualscreen";
static const char *k_str_general_settings_devices_capture_idx = "general_settings_devices_capture_idx";
static const char *k_str_general_settings_devices_color_resolution_idx = "general_settings_devices_color_resolution_idx";
static const char *k_str_general_settings_devices_depth_resolution_idx = "general_settings_devices_depth_resolution_idx";
static const char *k_str_general_settings_devices_depth_mapbits_idx = "general_settings_devices_depth_mapbits_idx";
static const char *k_str_general_settings_capture_howlong = "general_settings_capture_howlong";
static const char *k_str_general_settings_capture_always = "general_settings_capture_always";
static const char *k_str_general_settings_fes_width = "general_settings_facial_ellipse_width";
static const char *k_str_general_settings_fes_height = "general_settings_facial_ellipse_height";
static const char *k_str_general_settings_los_distance = "general_settings_line_of_sight_distance";
static const char *k_str_general_settings_los_outerangle = "general_settings_line_of_sight_outerangle";
static const char *k_str_general_settings_los_innerangle = "general_settings_line_of_sight_innerangle";
static const char *k_str_general_settings_ind_outer_points = "general_settings_indicator_outer_points";
static const char *k_str_general_settings_ind_outer_radius = "general_settings_indicator_outer_radius";
static const char *k_str_general_settings_ind_inner_points = "general_settings_indicator_inner_points";
static const char *k_str_general_settings_ind_inner_radius = "general_settings_indicator_inner_radius";
static const char *k_str_general_settings_fes_width_2 = "general_settings_facial_ellipse_width_2";
static const char *k_str_general_settings_fes_height_2 = "general_settings_facial_ellipse_height_2";
static const char *k_str_general_settings_los_distance_2 = "general_settings_line_of_sight_distance_2";
static const char *k_str_general_settings_los_outerangle_2 = "general_settings_line_of_sight_outerangle_2";
static const char *k_str_general_settings_los_innerangle_2 = "general_settings_line_of_sight_innerangle_2";
static const char *k_str_general_settings_ind_outer_points_2 = "general_settings_indicator_outer_points_2";
static const char *k_str_general_settings_ind_outer_radius_2 = "general_settings_indicator_outer_radius_2";
static const char *k_str_general_settings_ind_inner_points_2 = "general_settings_indicator_inner_points_2";
static const char *k_str_general_settings_ind_inner_radius_2 = "general_settings_indicator_inner_radius_2";
static const char *k_str_general_settings_automation_timeinterval = "general_settings_automation_timeinterval";
#endif
typedef enum E_DeviceType_Tag {
	e_devicetype_unknown = 0,
	e_devicetype_camera_common,
	e_devicetype_camera_depth_etron
} EDeviceType;

typedef enum E_ImageProcessing_Approach_Tag {
	e_imageprocessing_normal_preview = 0,
	e_imageprocessing_normal_capture,
	e_imageprocessing_etron_preview,
	e_imageprocessing_etron_capture
} EImageProcessingApproach;

typedef enum E_Component_State_Tag {
	e_component_state_idle = 0,
	e_component_state_initialized,
	e_component_state_stopped,
	e_component_state_started,
} EComponentState;

typedef enum E_TestingSession_State_Tag {
	e_testingsession_none = 0,
	e_testingsession_init = 1,
	e_testingsession_normal,
	e_testingsession_swinghead,
	e_testingsession_expression_wearglasses,
	e_testingsession_expression_wearhat,
	e_testingsession_expression_smile,
	e_testingsession_expression_laugh,
	e_testingsession_expression_angry,
	e_testingsession_expression_sad,
#ifdef SUPPORT_COVER_MOUTH
	e_testingsession_expression_covermouth,
#endif
	e_testingsession_normal_2,
	e_testingsession_swinghead_2,
	e_testingsession_expression_wearglasses_2,
	e_testingsession_expression_wearhat_2,
	e_testingsession_expression_smile_2,
	e_testingsession_expression_laugh_2,
	e_testingsession_expression_angry_2,
	e_testingsession_expression_sad_2,
#ifdef SUPPORT_COVER_MOUTH
	e_testingsession_expression_covermouth_2,
#endif
	e_testingsession_auto
} ETestingSession;

typedef struct S_Camera_ImgConverter_Config_Tag {
	int32_t owner_id;
	int32_t want_width;
	int32_t want_height;

} SCameraImageProcessingConfig;
#ifndef CALLER_IS_KNERON_FACE3DGRABBER
namespace KneronFace3DGrabber {

	typedef struct S_TestingSession_Table_Tag {
		ETestingSession testingsession;
		ETestingSession testingsession_next;
		size_t			num_of_pictures;
		size_t			num_of_pictures_previously;
		char			category_name[32];

	} STestingSessionTable;

	static STestingSessionTable g_testingsession_table[] = {
		{ e_testingsession_none, e_testingsession_init, 0, 0, ""},
		{ e_testingsession_init, e_testingsession_normal, 0, 0, "" },

		{ e_testingsession_normal, e_testingsession_swinghead, 0, 0, "angles" },
		{ e_testingsession_swinghead, e_testingsession_expression_wearglasses, 0, 0, "angles" },
		{ e_testingsession_expression_wearglasses, e_testingsession_expression_wearhat, 0, 0, "glasses" },
		{ e_testingsession_expression_wearhat, e_testingsession_expression_smile, 0, 0, "hat" },
		{ e_testingsession_expression_smile, e_testingsession_expression_laugh, 0, 0, "smile" },
		{ e_testingsession_expression_laugh, e_testingsession_expression_angry, 0, 0, "laugh" },
		{ e_testingsession_expression_angry, e_testingsession_expression_sad, 0, 0, "angry" },
	#ifdef SUPPORT_COVER_MOUTH
		{ e_testingsession_expression_sad, e_testingsession_expression_covermouth, 0, 0, "sad" },
		{ e_testingsession_expression_covermouth, e_testingsession_normal_2, 0, 0, "mouth" },
	#else
		{ e_testingsession_expression_sad, e_testingsession_normal_2, 0, 0, "sad" },
	#endif
		{ e_testingsession_normal_2, e_testingsession_swinghead_2, 0, 0, "angles" },
		{ e_testingsession_swinghead_2, e_testingsession_expression_wearglasses_2, 0, 0, "angles" },
		{ e_testingsession_expression_wearglasses_2, e_testingsession_expression_wearhat_2, 0, 0, "glasses" },
		{ e_testingsession_expression_wearhat_2, e_testingsession_expression_smile_2, 0, 0, "hat" },
		{ e_testingsession_expression_smile_2, e_testingsession_expression_laugh_2, 0, 0, "smile" },
		{ e_testingsession_expression_laugh_2, e_testingsession_expression_angry_2, 0, 0, "laugh" },
		{ e_testingsession_expression_angry_2, e_testingsession_expression_sad_2, 0, 0, "angry" },
	#ifdef SUPPORT_COVER_MOUTH
		{ e_testingsession_expression_sad_2, e_testingsession_expression_covermouth_2, 0, 0, "sad" },
		{ e_testingsession_expression_covermouth_2, e_testingsession_normal, 0, 0, "mouth" },
	#else
		{ e_testingsession_expression_sad_2, e_testingsession_normal, 0, 0, "sad" },
	#endif
		{ e_testingsession_auto, e_testingsession_auto, false, false, 0, 0 }
	};

	class GlobalCacheData 
#ifdef QT_VERSION
	: public QObject 
	{
		Q_OBJECT
#else
	{
#endif
	public:
		GlobalCacheData()
			: scene_width(0)
			, scene_height(0)
			, devices_dualscreen(false)
			, devices_capture_idx(0)
			, devices_color_resolution_idx(0)
			, devices_depth_resolution_idx(0)
			, devices_depth_mapbits_idx(2)
			, capture_howlong(120)
			, capture_always(true)
			, facial_ellipse_width(150)
			, facial_ellipse_height(200)
			, los_distance(50)
			, los_outer_angle(15)
			, los_inner_angle(8)
			, ind_outer_points(8)
			, ind_outer_radius(0)
			, ind_inner_points(4)
			, ind_inner_radius(0)
			, facial_ellipse_width_2(195)
			, facial_ellipse_height_2(260)
			, los_distance_2(30)
			, los_outer_angle_2(15)
			, los_inner_angle_2(8)
			, ind_outer_points_2(8)
			, ind_outer_radius_2(0)
			, ind_inner_points_2(4)
			, ind_inner_radius_2(0)
			, current_snapshot_idx(0)
			, current_testing_session(e_testingsession_none)
		{
		#ifdef QT_VERSION
			application_dir = QApplication::applicationDirPath();
			settings_file_path = application_dir + "/KneronFace3DGrabber.ini";
		#endif
		}
		void InitParameters()
		{
		#ifdef QT_VERSION
			QString str;
			QSettings settings(settings_file_path, QSettings::IniFormat);

			settings.setIniCodec("UTF-8");

			devices_dualscreen = settings.value(QString(k_str_general_settings_devices_dualscreen), false).toBool();
			devices_capture_idx = settings.value(QString(k_str_general_settings_devices_capture_idx), 0).toInt();
			
			devices_color_resolution_idx = settings.value(QString(k_str_general_settings_devices_color_resolution_idx), 0).toInt();
			devices_depth_resolution_idx = settings.value(QString(k_str_general_settings_devices_depth_resolution_idx), 0).toInt();
			devices_depth_mapbits_idx = settings.value(QString(k_str_general_settings_devices_depth_mapbits_idx), 0).toInt();

			capture_howlong = settings.value(QString(k_str_general_settings_capture_howlong), 120).toInt();
			capture_always = settings.value(QString(k_str_general_settings_capture_always), true).toBool();

			QString strOuter1;
			QString strInner1;
			QString strOuter2;
			QString strInner2;

			{
				facial_ellipse_width = settings.value(QString(k_str_general_settings_fes_width), 150).toInt(); 
				facial_ellipse_height = settings.value(QString(k_str_general_settings_fes_height), 200).toInt(); 
				los_distance = settings.value(QString(k_str_general_settings_los_distance), 50).toInt(); 
				los_outer_angle = settings.value(QString(k_str_general_settings_los_outerangle), 15).toInt();
				los_inner_angle = settings.value(QString(k_str_general_settings_los_innerangle), 8).toInt(); 
			}
			{
				facial_ellipse_width_2 = settings.value(QString(k_str_general_settings_fes_width_2), 195).toInt(); 
				facial_ellipse_height_2 = settings.value(QString(k_str_general_settings_fes_height_2), 260).toInt(); 
				los_distance_2 = settings.value(QString(k_str_general_settings_los_distance_2), 30).toInt();
				los_outer_angle_2 = settings.value(QString(k_str_general_settings_los_outerangle_2), 15).toInt(); 
				los_inner_angle_2 = settings.value(QString(k_str_general_settings_los_innerangle_2), 8).toInt(); 
			}
			//qDebug("InitParameters facial_ellipse_width=%d", facial_ellipse_width);
			//qDebug("InitParameters facial_ellipse_height=%d", facial_ellipse_height);
			//qDebug("InitParameters los_distance=%d", los_distance);
			//qDebug("InitParameters los_outer_angle=%d", los_outer_angle);
			//qDebug("InitParameters los_inner_angle=%d", los_inner_angle);
			//qDebug("InitParameters facial_ellipse_width_2=%d", facial_ellipse_width_2);
			//qDebug("InitParameters facial_ellipse_height_2=%d", facial_ellipse_height_2);
			//qDebug("InitParameters los_distance_2=%d", los_distance_2);
			//qDebug("InitParameters los_outer_angle_2=%d", los_outer_angle_2);
			//qDebug("InitParameters los_inner_angle_2=%d", los_inner_angle_2);
			{
				int distance = los_distance;
				int outerAngle = los_outer_angle;
				int innerAngle = los_inner_angle;
				int outerSize = distance * qTan(outerAngle * M_PI / 180);
				int innerSize = distance * qTan(innerAngle * M_PI / 180);
				QScreen *srn = QApplication::screens().at(0);
				int screenWidth = srn->geometry().width();
				float physicalWidth = srn->physicalSize().width();

				strOuter1.sprintf("%d", (int)((float)(screenWidth * outerSize * 10) / physicalWidth)); // 1920 * 13 * 10/344
				strInner1.sprintf("%d", (int)((float)(screenWidth * innerSize * 10) / physicalWidth));
				//qDebug("InitParameters strOuter1=%s", strOuter1.toLatin1().constData());
				//qDebug("InitParameters strOuter1=%s", strInner1.toLatin1().constData());
			}
			{
				int distance = los_distance_2;
				int outerAngle = los_outer_angle_2;
				int innerAngle = los_inner_angle_2;
				int outerSize = distance * qTan(outerAngle * M_PI / 180);
				int innerSize = distance * qTan(innerAngle * M_PI / 180);
				QScreen *srn = QApplication::screens().at(0);
				int screenWidth = srn->geometry().width();
				float physicalWidth = srn->physicalSize().width();

				strOuter2.sprintf("%d", (int)((float)(screenWidth * outerSize * 10) / physicalWidth)); // 1920 * 13/344
				strInner2.sprintf("%d", (int)((float)(screenWidth * innerSize * 10) / physicalWidth));
				//qDebug("InitParameters strOuter1=%s", strOuter2.toLatin1().constData());
				//qDebug("InitParameters strOuter1=%s", strInner2.toLatin1().constData());
			}

			{
				//los_distance = settings.value(QString(k_str_general_settings_los_distance), 50).toInt();
				//los_outer_angle = settings.value(QString(k_str_general_settings_los_outerangle), 15).toInt();
				//los_inner_angle = settings.value(QString(k_str_general_settings_los_innerangle), 8).toInt();
				ind_outer_points = settings.value(QString(k_str_general_settings_ind_outer_points), 8).toInt();
				ind_outer_radius = settings.value(QString(k_str_general_settings_ind_outer_radius), strOuter1.toInt()).toInt();
				ind_inner_points = settings.value(QString(k_str_general_settings_ind_inner_points), 4).toInt();
				ind_inner_radius = settings.value(QString(k_str_general_settings_ind_inner_radius), strInner1.toInt()).toInt();
				//qDebug("InitParameters ind_outer_points=%d", ind_outer_points);
				//qDebug("InitParameters ind_outer_radius=%d", ind_outer_radius);
				//qDebug("InitParameters ind_inner_points=%d", ind_inner_points);
				//qDebug("InitParameters ind_inner_radius=%d", ind_inner_radius);
			}
			{
				//los_distance_2 = settings.value(QString(k_str_general_settings_los_distance_2), 50).toInt();
				//los_outer_angle_2 = settings.value(QString(k_str_general_settings_los_outerangle_2), 15).toInt();
				//los_inner_angle_2 = settings.value(QString(k_str_general_settings_los_innerangle_2), 8).toInt();
				ind_outer_points_2 = settings.value(QString(k_str_general_settings_ind_outer_points_2), 8).toInt();
				ind_outer_radius_2 = settings.value(QString(k_str_general_settings_ind_outer_radius_2), strOuter2.toInt()).toInt();
				ind_inner_points_2 = settings.value(QString(k_str_general_settings_ind_inner_points_2), 4).toInt();
				ind_inner_radius_2 = settings.value(QString(k_str_general_settings_ind_inner_radius_2), strInner2.toInt()).toInt();
				//qDebug("InitParameters ind_outer_points_2=%d", ind_outer_points_2);
				//qDebug("InitParameters ind_outer_radius_2=%d", ind_outer_radius_2);
				//qDebug("InitParameters ind_inner_points_2=%d", ind_inner_points_2);
				//qDebug("InitParameters ind_inner_radius_2=%d", ind_inner_radius_2);
			}
			current_snapshot_idx = 0;
			current_testing_session = e_testingsession_init;

			str = settings.value(QString(k_str_mainwindow_prefix), "112233445566").toString();
			prefix = str;

			recording_file_path = application_dir;
			recording_file_path += "/";
			recording_file_path += prefix;


			str = settings.value(QString(k_str_mainwindow_location), "Indoor").toString();
			location = str;

			memcpy(testingsession_table, g_testingsession_table, sizeof(g_testingsession_table));

			this->UpdateTestingSessionParameters();
		#endif
		}

		size_t QueryHowManyPicturesWillTaken(ETestingSession testingSession)
		{
			size_t ret = 0;
			for (int i = 0; i < DIM(testingsession_table); ++i)
			{
				if (testingSession == testingsession_table[i].testingsession)
				{
					ret = testingsession_table[i].num_of_pictures;
					break;
				}
			}

			return ret;
		}

		int QueryLastestPictureIdx(ETestingSession testingSession)
		{
			size_t ret = 0;
			int i;
			for (i = 0; i < DIM(testingsession_table); ++i)
			{
				if (testingSession == testingsession_table[i].testingsession)
				{
					ret = testingsession_table[i].num_of_pictures_previously;
					break;
				}
			}

			return static_cast<int>(ret);
		}

		STestingSessionTable* GetTestingSessionInfo(ETestingSession testingSession)
		{
			STestingSessionTable *pTable = nullptr;
			for (int i = 0; i < DIM(testingsession_table); ++i)
			{
				if (testingSession == testingsession_table[i].testingsession)
				{
					pTable = &testingsession_table[i];
					break;
				}
			}

			return pTable;
		}

		void UpdateTestingSessionParameters()
		{
			size_t totalPictureCnt = 0;
			for (int i = 0; i < DIM(testingsession_table); ++i)
			{
				switch (testingsession_table[i].testingsession)
				{
				case e_testingsession_normal:
					if (los_distance > 30)
						testingsession_table[i].num_of_pictures = 1 + ind_outer_points + ind_inner_points;
					else
						testingsession_table[i].num_of_pictures = 1 + ind_outer_points;// ind_inner_points;
					break;
				case e_testingsession_normal_2:
					if (los_distance_2 > 30)
						testingsession_table[i].num_of_pictures = 1 + ind_outer_points + ind_inner_points;
					else
						testingsession_table[i].num_of_pictures = 1 + ind_outer_points;// ind_inner_points;
					break;
				case e_testingsession_swinghead:
				case e_testingsession_swinghead_2:
					testingsession_table[i].num_of_pictures = 2;
					break;
				case e_testingsession_expression_wearglasses:
				case e_testingsession_expression_wearglasses_2:
				case e_testingsession_expression_wearhat:
				case e_testingsession_expression_wearhat_2:
				case e_testingsession_expression_smile:
				case e_testingsession_expression_smile_2:
				case e_testingsession_expression_laugh:
				case e_testingsession_expression_laugh_2:
				case e_testingsession_expression_angry:
				case e_testingsession_expression_angry_2:
				case e_testingsession_expression_sad:
				case e_testingsession_expression_sad_2:
			#ifdef SUPPORT_COVER_MOUTH
				case e_testingsession_expression_covermouth:
				case e_testingsession_expression_covermouth_2:
			#endif
					testingsession_table[i].num_of_pictures = 1 + ind_inner_points;
					break;
				default:;
				}

				totalPictureCnt += testingsession_table[i].num_of_pictures;
				testingsession_table[i].num_of_pictures_previously = totalPictureCnt;
				//qDebug("UpdateTestingSessionParameters testingsession_table[%d].testingsession=%d", i, testingsession_table[i].testingsession);
				//qDebug("UpdateTestingSessionParameters testingsession_table[%d].num_of_pictures=%d", i, testingsession_table[i].num_of_pictures);
				//qDebug("UpdateTestingSessionParameters testingsession_table[%d].num_of_pictures_previously=%d", i, testingsession_table[i].num_of_pictures_previously);
			}
		}
	#ifdef OWNER_IS_KNERON_FACE3DGRABBER
		void UpdatePrefix(QString str)
		{
			recording_file_path = application_dir;
			recording_file_path += "/";
			recording_file_path += str;

			prefix = str;
		}
	
		static GlobalCacheData* instance()
		{
			return Singleton<GlobalCacheData>::instance(GlobalCacheData::createInstance);
		}
	#endif
#ifdef QT_VERSION
	public slots:
#else
	public:
#endif
		void handleTestingSessionFinished()
		{
			//qDebug("GlobalCacheData::handleTestingSessionFinished current_testing_session=%d", current_testing_session);

			for (int i = 0; i < DIM(testingsession_table); ++i)
			{
				if (current_testing_session == testingsession_table[i].testingsession)
				{
					current_testing_session = testingsession_table[i].testingsession_next;
					//qDebug("GlobalCacheData::handleTestingSessionFinished new current_testing_session=%d", current_testing_session);
					break;
				}
			}
		}

	public:
		size_t scene_width;
		size_t scene_height;
		std::vector<std::string> video_device_name_list;
		std::vector<std::string> audio_device_name_list;
		bool devices_dualscreen;
		int devices_capture_idx;
		int devices_color_resolution_idx;
		int devices_depth_resolution_idx;
		int devices_depth_mapbits_idx;
		int capture_howlong;
		bool capture_always;
		int facial_ellipse_width;
		int facial_ellipse_height;
		int los_distance;
		int los_outer_angle;
		int los_inner_angle;
		int ind_outer_points;
		int ind_outer_radius;
		int ind_inner_points;
		int ind_inner_radius;
		int facial_ellipse_width_2;
		int facial_ellipse_height_2;
		int los_distance_2;
		int los_outer_angle_2;
		int los_inner_angle_2;
		int ind_outer_points_2;
		int ind_outer_radius_2;
		int ind_inner_points_2;
		int ind_inner_radius_2;
		int32_t current_snapshot_idx;
		ETestingSession current_testing_session;
	#ifdef QT_VERSION
		QString application_dir;
		QString settings_file_path;
		QString recording_file_path;
		QString prefix;
		QString location;
	#endif
		STestingSessionTable testingsession_table[DIM(g_testingsession_table)];

	private:
		static GlobalCacheData* createInstance()
		{
			return new GlobalCacheData();
		}
	};
}
#endif
#endif // KNERONFACEGRABBER_H
