#include "common.h"


std::string GetCurrentModuleFolder()
{
    char filename[MAX_PATH] = { '\0' };
    GetModuleFileNameA(NULL, filename, MAX_PATH);
    std::string sFilename(filename);
    return sFilename.substr(0, sFilename.find_last_of("\\/"));
}
