#ifndef COMMON_H
#define COMMON_H


#include <windows.h>
#include <iostream>
#include <string>
#include <queue>
#include <mutex>

#ifdef QT_VERSION
#define DSG(mode, __format__, ...) { if (1 == mode) qDebug(__format__, ##__VA_ARGS__); }
#else
#ifdef __linux__
#define DSG(mode, __format__, ...) { if (1 == mode) printf(__format__, ##__VA_ARGS__); }
#elif _WIN32
#include <windows.h>
#define MAX_DBG_MSG_LEN (1024)
static void DSG(int mode, const char * format, ...)
{
	if (1 == mode) {
		char buf[MAX_DBG_MSG_LEN];
		va_list ap;
		va_start(ap, format);
		_vsnprintf(buf, sizeof(buf), format, ap);
		va_end(ap);
		OutputDebugStringA(buf);
	}
}
#endif
#endif


class CCritSec {

    // make copy constructor and assignment operator inaccessible

    CCritSec(const CCritSec &refCritSec);
    CCritSec &operator=(const CCritSec &refCritSec);

    CRITICAL_SECTION m_CritSec;

#ifdef DEBUG
public:
    DWORD   m_currentOwner;
    DWORD   m_lockCount;
    BOOL    m_fTrace;        // Trace this one
public:
    CCritSec();
    ~CCritSec();
    void Lock();
    void Unlock();
#else

public:
    CCritSec() {
        InitializeCriticalSection(&m_CritSec);
    };

    ~CCritSec() {
        DeleteCriticalSection(&m_CritSec);
    };

    void Lock() {
        EnterCriticalSection(&m_CritSec);
    };

    void Unlock() {
        LeaveCriticalSection(&m_CritSec);
    };
#endif
};

class CAutoLock {

    // make copy constructor and assignment operator inaccessible

    CAutoLock(const CAutoLock &refAutoLock);
    CAutoLock &operator=(const CAutoLock &refAutoLock);

protected:
    CCritSec * m_pLock;

public:
    CAutoLock(CCritSec * plock)
    {
        m_pLock = plock;
        m_pLock->Lock();
    };

    ~CAutoLock() {
        m_pLock->Unlock();
    };
};

template <typename T>
class ThreadSafeQueue {
public:
	void Insert(T value);
	void Popup(T &value);
	bool Empty() const;

private:
	mutable std::mutex m_mutex;
	std::queue<T> m_queue;
	std::condition_variable m_cond;
};

template <typename T>
void ThreadSafeQueue<T>::Insert(T value) {
	std::lock_guard<std::mutex> mutex(m_mutex);
	//DSG(1, "Insert m_queue.size()=%d", m_queue.size());
	if (m_queue.size() > 30)
	{
		m_queue.pop();
	}

	m_queue.push(value);
	m_cond.notify_one();
}

template <typename T>
void ThreadSafeQueue<T>::Popup(T &value) {
	std::unique_lock<std::mutex> mutex(m_mutex);
	//DSG(1, "Popup m_queue.size() 1 =%d funcname=%s", m_queue.size(), __FUNCTION__);
	m_cond.wait(mutex, [this] {return !m_queue.empty(); });

	value = m_queue.front();
	m_queue.pop();
	//DSG(1, "Popup m_queue.size() 2 =%d funcname=%s", m_queue.size(), __FUNCTION__);
}

template <typename T>
bool ThreadSafeQueue<T>::Empty() const {
	std::lock_guard<std::mutex> mutex(m_mutex);
	return m_queue.empty();
}

class ThreadRAII
{
public:
	explicit ThreadRAII(std::thread &t) : m_thread(t) {}
	~ThreadRAII()
	{
		DSG(1, "~ThreadRAII");
		if (this->m_thread.joinable())
			this->m_thread.join();
	}
	ThreadRAII(const ThreadRAII&) = delete;
	ThreadRAII& operator= (const ThreadRAII&) = delete;

private:
	std::thread &m_thread;
};


std::string GetCurrentModuleFolder();

#endif // COMMON_H
