#include "camera_api_etron.h"
#include "./etron/depthcamera.h"
#include "./etron/recordmanager_etron.h"

std::thread threadVideoDevice;
DepthCamera *m_pDepthCamera = nullptr;
ThreadRAII *m_pDepthCameraThread = nullptr;


int OpenCamera_ETron(int deviceIdx)
{
	int ret = -1;
	if (!m_pDepthCamera)
	#ifdef QT_VERSION
		m_pDepthCamera = new DepthCamera(nullptr);
	#else
		m_pDepthCamera = new DepthCamera();
	#endif
	if (m_pDepthCamera)
	{
		SCameraConfig depthConfig;
		depthConfig.device_idx = deviceIdx;
		depthConfig.interval_ms = 33;
		//DSG(1, "MainWindow::OpenCamera depthConfig.device_idx=%d", depthConfig.device_idx);

		if (m_pDepthCamera->SetConfig(&depthConfig) && m_pDepthCamera->Open())
		{
			ret = 0;

			{
				AbstractCamera *p = m_pDepthCamera;
				//Base* b = new Derived();
				//std::thread t(std::ref(*b));
				threadVideoDevice = std::thread(std::ref(*p));
				m_pDepthCameraThread = new ThreadRAII(threadVideoDevice);
				DSG(1, "OpenCamera_ETron ready go");
				//t.join();
				//ThreadTest(*b);
				//std::thread threadVideoDevice{ c };
				////std::thread threadVideoDevice{ m_pDepthCamera };
				//std::thread threadVideoDevice = std::thread(CameraThread, m_pDepthCamera);

			}
		}
	}

	return ret;
}

void CloseCamera_ETron()
{
	if (m_pDepthCamera)
	{
		delete m_pDepthCamera;
		m_pDepthCamera = nullptr;
	}

	if (m_pDepthCameraThread)
	{
		delete m_pDepthCameraThread;
		m_pDepthCameraThread = nullptr;
	}
}

void StartCamera_ETron()
{
	DSG(1, "StartCamera_ETron");
	if (m_pDepthCamera)
	{
		DSG(1, "StartCamera_ETron 2");
		m_pDepthCamera->start();
	}
}

void StopCamera_ETron()
{
	if (m_pDepthCamera)
	{
		m_pDepthCamera->stop();
		Sleep(2000);
	}
}

void SaveCameraFrame_ETron_PreviewBmp(const cv::Mat &matSrc, const char *filename)
{
	if (m_pDepthCamera)
		RecordingManager_ETron_SaveToBmp(static_cast<void*>(m_pDepthCamera), matSrc, filename);
}

void SaveCameraFrame_ETron_DepthBmp(const cv::Mat &matSrc, const char *filename)
{
	if (m_pDepthCamera)
		RecordingManager_ETron_ConvertToBmp24Bits(static_cast<void*>(m_pDepthCamera), matSrc, filename);
}

void SaveCameraFrame_ETron_DepthRaw(const cv::Mat &matSrc, const char *filename)
{
	if (m_pDepthCamera)
		RecordingManager_ETron_ConvertToRaw(static_cast<void*>(m_pDepthCamera), matSrc, filename);
}

void GetPreviewImage_ETron(cv::Mat &matImage)
{
	if (m_pDepthCamera)
		m_pDepthCamera->GetPreviewImage(matImage);
}

void GetDepthImage_ETron(cv::Mat &matImage)
{
	if (m_pDepthCamera)
		m_pDepthCamera->GetDepthImage(matImage);

}

